# Linux en servidores:

Un poco de historia:

En 1983, Richard Stallman inicia el proyecto GNU (GNU’s Not Unix) para crear un sistema operativo Unix basado en software libre.

• Linus Torvalds creó el kernel de Linux en 1992 a partir de MINIX.

• El kernel de Linux junto herramientas del proyecto GNU, generó la primera versión del sistema operativo GNU/Linux.

• A partir de esta combinación nacen varias de distribuciones:

- Debian o basadas en Debian (Ubuntu, Linux Mint, etc.)
- Red Hat o basadas en Red Hat (Fedora, SUSE, etc.)
- Otras, pero las anteriores son las más importantes.

## Ubuntu:

Distribución que vamos a utilizar.

• Distribución creada a partir de Debian:

• Su objetivo es hacer Debian más fácil de utilizar.

• Soportada por Canonical, empresa de Mark Shuttleworth.

• Dos releases anuales:

• En Abril: YEAR.04 (14.04, 15.04, etc.)

• En Octubre: YEAR.10 (14.10, 15.10, etc.)

• Tiene releases LTS: Long Term Support.

• Cuenta con diferentes versiones: Desktop, Server, Education, etc.

• La cuenta root está deshabilitada por seguridad. se accede por medio de un usurio que luego lo hacemos root.

## Empezamos con lo básico:

En linux y en servidores de linux tenemos que usar la terminal. El bash. Una aplicación que usamos para meter instrucciones que ekecutara el servidor. Existen diferentes tipos de terminales, la mas usada es bash, existe sh, zsh, etc.

• Los servidores linux no suelen disponer de interfaz gráfica.

• La administración se realiza a través del intérprete de comandos.

• El intérprete más utilizado en Linux es Bash.

• Dispone de un pequeño lenguaje de programación para hacer scripts.

## Conectarnos al servidor:

• Generalmente, conectamos a servidores Linux de manera remota.

• Para conexión remota, se utiliza el protocolo SSH (Secure SHell).

• Es muy seguro y permite también transferencia de archivos (SFTP: SSH File Transfer Protocol).

• Presente en Linux y Mac.

• En Windows hay que usar PuTTy.

## Como conectarnos al servidor:

```bash
ssh user@host
```

ssh -> el protocolo,
user -> el usuario,
host -> el host donde esta el servidor.

El AWS cambia un poco, porque no tenemos una contraseña, tenemos es la llave.

## Conectarnos a nuestro servidor de AWS:

Abrimos la consola de comandos. Nos vamos a la carpeta donde tenemos descargado el .pem. Lo tenemos guardado en la carpeta del modulo. web-kc.pem

comando para saber donde estamos:

```bash
pwd
```

Vamos a proteger nuestro archivo web-kc.pem, para que si nos conectamos entre bien en linux, en windows ha entrado sin problema. Con esto solo damnos permisos a mi para usar este archivo. En mi ordenador.

```bash
chmod 600 web-kc.pem
```

Como usuario ponemos ubuntu, viene en la documentación de AWS. y la IP la sacamos del panel de AWS:

![](../doc/img/ip.png)

### Nos conectamos al servidor:

```bash
ssh -i web-kc.pem ubuntu@23.20.123.126
```

Nos conectamos sin problema. Ya estamos conectado al servidor.

![](../doc/img/server.png)

Para salir del servidor:

```bash
logout
```

```bash
exit
```

```bash
CTRL + D
```

## Vamos a ver algunos comandos:

## Vemos donde estamos:

```bash
pwd
```

/home/ubuntu

Como vemos el usuario es ubuntu, estamos en la carpeta ubuntu.

```bash
whoami
```

Tambien nos dice el usuario que somos: ubuntu.

## Para saber la hora del servidor:

```bash
date
```

## Para saber quien esta conectado al servidor:

```bash
who
```

## Vamos a dar un paseo por el sistema:

Para listar archivos del directorio local:

```bash
ls
```

Para listar archivos del directorio que le indicamos:

```bash
ls <path>
```

Para listar archivos del directorio que le indicamos en modo lista:

```bash
ls -l <path>
```

Para crear un archivo vacio:

```bash
touch hello
```

Para movernos por carpetas: Volver al directorios anterios

```bash
cd ..
```

Para movernos al directorio indicado:

```bash
cd <path>
```

Para volver al usuario, al home:

```bash
cd
```

## Descripcion de lo que vemos por linea de comando:

![](../doc/img/ubuntu.png)

Accesos directos: los que comienzan por l.
Directorios: los que comienzan por d.

## Sistema de archivos:

- A diferencia de Windows, en Linux el sistema de ficheros no diferencia entre unidades (C:, D:, etc.).

- En las rutas se utiliza el slash “/“ para indicar cambio de directorio (al contrario que en Windows, que es el backslash “\”).

- La raíz del sistema es “/“ y a partir de ahí hay diferentes directorios.

- No son necesarias las extensiones en los archivos.

Este es el sistema de fichero de linux:

/
/bin
/boot
/dev
/etc
/home
/lib
/lost+found
/media
/mnt
/opt
/proc
/root
/sbin
/srv
/sys
/tmp
/usr
/var

Nosotros nos centraremos en las siguientes:

/etc
/home
/root
/sbin
/tmp
/usr
/var

## carpeta /etc:

- Contiene ficheros de configuración del sistema.
- Scripts que se ejecutan cuando arranca el sistema en:

- /etc/init.d/
- /etc/rc.d/

## carpeta /home y carpeta /root:

- Home de cada usuario del sistema: Por defecto estan aqui.
  - /home/goku
  - /home/vegeta
  - /home/mutenroshi

Cuando nos conectamos por SSH, por defecto accedemos a nuestro carpeta /home

- El /root es la casa de root (superadministrador)

## carpeta /sbin:

Almacena ejecutables de comandos que sólo pueden ser utilizados por un usuario super-administrador. Esto quiere decir programas.

## carpeta /tmp:

- Directorio temporal que permite a los usuarios almacenar datos.
- Los datos serán eliminados cuando el sistema se reinicio o necesite espacio.

## carpeta /usr:

Contiene subdirectorios con ejecutables del sistema:

- /usr/bin: ejecutables para cualquier usuario
- /usr/lib: librerías de programación (C/C++ habitualmente)
- /usr/local: archivos locales (desarrollados por ti)
- /usr/sbin: ejecutables sólo para administradores
- /usr/share: datos compartidos (documentación)
- /usr/src: código fuente del kernel de Linux

## carpeta /var:

Contiene varios subdirectorios y archivos con diferentes tipos de información:

- /var/log: archivos de log
- /var/mail: buzones de e-mail de los usuarios
- /var/run: descriptores de procesos o sockets
- /var/www: archivos servidos por el servidor web

## Jugando con comandos:

### Crear carpetas:

```bash
mkdir <foldername>
```

### Eliminar directorio o carpetas: NO LO MANDA A LA PAPELERA!!!

```bash
rmdir <foldername>
```

OJO si el directorio tiene archivos usaremos:

```bash
rm -rf <foldername>
```

Peligro!!!: el siguiente comando borra todo el disco duro:

```bash
rm -rf /
```

### Crear archivos vacios:

```bash
touch <filename>
```

### Editar archivos:

```bash
nano <filename>
```

```bash
vi <filename>
```

A veces nano no esta disponible en algunas distribuciones por defecto. Sin embargo, vi si suele estar instalado siempre, pero es mas dificil de usar.

### Ver el contenido de un archivo:

Muestra el contenido de un fichero(ideal para archivos pequeños):

```bash
cat <filename>
```

Permite leer un fichero grande poco a poco:

```bash
more <filename>
```

### Cuantas palabras/lineas tiene:

Cuantas palabras tiene un archivo de texto:

```bash
wc <filename>
```

Cuantas lineas tiene un archivo de texto:

```bash
wc -l <filename>
```

### Diferencias entre archivos:

Muestra la diferencias entre dos archivos:

```bash
dif <file1> <file2>
```

### Para ordenar:

Ordena las lineas de una archivo de texto:

```bash
short <filename>
```

### Para filtrar:

Buscar <query> en <filename>. Permite expresiones regulares:

```bash
grep <query> <filename>
```

### Para copiar archivos:

Copia un archivo de un sitio a otro:

```bash
cp <source> <target>
```

### Para mover/renombrar archivos:

Mueve o renombra un archivo:

```bash
mv <source> <target>
```

### Para pedir ayuda:

Muestra el manual del programa o comando:

```bash
man <command name>
```

### Para buscar archivos: Comando muy potente:

Buscar un archivo de nombre filename en el directorio actual y subdirectorios:

```bash
find . -name <filename>
```

Busca un archivo de nombre filename en todo el sistema

```bash
find / -name <filename>
```

Tenemos en esta web un poco mas fino para usar este comando ya que es muy potente:

https://www.tecmint.com/35-practical-examples-of-linux-find-command/
