# AWS - Amazon Web Services.

Actualmente es la mas famosa y lider plataforma cloud del mundo. Amazon gana a dia de hoy mas dinero con AWS que con el marketplace. AWS nacio en un unicio para dar servicio solo para ellos. Quedo tan bien montado que optaron por comercializarlo.

AWS es una colección de servicios, cada vez hay mas y mas. Nosotros vamos a trabajar con el servicio EC2, lo encontramos en los servicios en el apartado de Informatica.

El servicio de EC2 es un servicio de mauinas virtuales en la nube. Vamos a montar un servidor y dentro de esta una base de datos.

Aqui el coste que vamos encontrar en AWS es pago por uso. Si usamos una maquina virtual por una hora, solo pagamos esa hora. Igual en el servicio de almacenamiento, todo pago por uso. Asi es bastante complicado saber cuanto pagaremos al mes.

Como calculamos esto. En AWS hay una calculadora de precios.

https://calculator.aws/#/

Vamos a calcular nuestro presupuesto, una estimación.

![](../doc/img/aws.png)

Buscamos el servicio de EC2 y le damos a configurar:

![](../doc/img/aws2.png)
![](../doc/img/aws3.png)

A continuacion vemos que las instancias comienzan por una letra, y luego un numero, eso es la generacion siempre optar por las de ultima generación. t4g.nano por ejemplo. Son mas baratas y ofrecen mejor rendimiento. Hacemos la seleccion de una memoria de 4gb y seleccionamos la t4g.medioun y vemos que tiene un coste de 0.0336 por hora. Tambien vemos el coste total mensual.

![](../doc/img/aws4.png)

Ahora estas instancias de EC2 vienen sin disco duro. Lo vemos donde dice almacenamiento EBS only, algunas como la m6gd.mediun si trae, lo vemos en la imagen. que viene con un disco NVMe de 59 GB. Pero esta almacenamiento de estas instancias es efimero. Es una almacenamiento para archivos temporales. al apagar la instancia esos datos se borraran.

Ahora tenemos 24.53 dolares al mes solo del servidor y ahora tenemos que poner el disco duro aparte. El disco duro lo metemos con lo que llaman Amazon Elastic Block Store(EBS). Lo vemos mas abajo. Tambien debemos seleccionar que sea en opciones de pago bajo demanda.

![](../doc/img/aws4.1.png)

Elegimos 50 GB. Las frecuencias de instantaneas son las copias de seguridad, le ponemos que sea diario y 300 MB. Con estos cambios ya vemos que ha subido al mes por 31.25 dolares.

![](../doc/img/aws5.png)

Esto seria a grandes rasgos el precio del servidor.

La copia de seguridad es como lo hace apple en su sistema de copia de seguridad.

Una cosa este precio es un precio bajo demanda, amazon quiero un servidor y lo quiero ahora y te lo da. Por eso como es un precio bajo demanda por eso es mas caro que muchos otros servicios que vemos por internet.

Tenemos unos trucos para ahorrar algo de dinero. Lo vemos en las opciones de pago. Podemos cambiar los diferentes planes y no ser bajo demanda, podemos tomar varios compromisos con amazon, podemos escoger la opcion EC2 Instance Saving Plans o Compute Saving Plans. Vemos como varian los precios en funcion d elos compromisos que adquiramos con Amazon.

Ir a demanda es mas caro que escoger un plan con un compromiso con amazon.

Aqui vamos a aprender a manejar EC2 de manera profesiona. Ya con la calculadora podemos calcular los que queramos y sabremos el coste que tendremos. Lo vemos tambien para el calcula de la bd.
