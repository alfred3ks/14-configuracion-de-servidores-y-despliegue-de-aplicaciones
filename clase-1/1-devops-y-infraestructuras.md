# DevOps y infraestructuras:

Tenemos un pdf en doc, donde esta la clase.

¿Qué es DevOps?

Es una metodología de desarrollo de software que se centra en la comunicación, colaboración, integración y automatización entre los desarrolladores de software y profesionales de IT.

![](../doc/img/devops.png)

¿Por qué DevOps?

• Aparición de las metodologías ágiles y lean.
• El desarrollo ahora es más rápido, pero no se puede poner tan rápido en producción.
• Para poder realizar despliegues continuos (continuous delivery o continuous deployment) es necesario acercar a desarrolladores(devs) y personal de operaciones IT (ops).
• DevOps nace como una necesidad de acercar los mundos del desarrollo y los sistemas para mejorar la competitividad.

En este curso vamos a aprender a manejar servidores. Como un devops. De developer a devops. Trabajaremos con Linux y la consola de comandos.

Este módulo cierra el circulo del bootcamp.

# Ejemplos de Infraestructuras: Design Systems - Diseño de sistemas.

Dentro de un servidor vamos a encontrar aplicaciones que suelen llamarse como por ejemplo servidor de BD, servidor de aplicación, servidor de correo, servidor web, estas aplicaciones deben estas siempre en marcha 24x7. Por eso se llaman aplicaciones de servidor. A continuacion podemos ver la infraestructura mas sencilla que podemos encontrar y para nuestro caso montaremos algo asi, salvo la parte del correo:

![](../doc/img/servidor.png)

Con este servidor basico una vez que tenmos todo instalado y pasa el tiempo vemos que se nos queda pequeño debido al trafico, pues debemos aumentar nuestro servidor, llamamos y podimos que no lo aumenten con el respectivo precio claro esta. A esto se le llama **escalado vertical.** Es cuando la mquina que tenemos la hacemos mas grande, aumentando esa maquina en disco duro, ram, cpu. Esto tiene un limite. Llegara el momento que esa máquina no va a crecer más. Para eso ya lo que nos toca es meter más máquinas.

![](../doc/img/servidor2.png)

Como vemos tenemos una maquina para cada servidor. Pero manteniendo la misma infraestructura.

Vemos que nuestra aplicación sigue creciendo en trafico y usuarios y esta infraestructura se nos queda pequeña. Para poder soportar el trafico podemos meter mas servidores con un balanceador de carga:

![](../doc/img/servidor3.png)

Un balanceador de carga es un repartidor de trafico. A el le llegan las peticiones, este esta interconectado a los servidores y sabe si responden o no. Asi reparte la carga a cada uno de ellos en funcion si estan disponibles o no. A esto le se le llama **escalado horizontal.** Si hacen falta mas maquinas de servidor web se pueden meter y luego desechar para momentos puntuales como por ejemplo el black friday.

En este modelo anterior vemos que el servidor de base de datos es uno solo, antes le venian las peticiones de un solo servidor ahora vienen de tres o mas servidores y empezara a sufrir, lo ideal es ampliarlo a cluster de BD.

![](../doc/img/cluester.png)

Esta infraestructura es bastante costosa y para ahorrar podriamos agregar un cdn de archivos estaticos. Archivos de Imagenes, css, javascript que se ejecuta en el navegador, etc. Estos servidores son muchos mas baratos.

![](../doc/img/cdn.png)

El almacenamiento de archivos debe estar en una fuente de terceros. Nunca dentro de nuestro servidor. En un CDN de archivos estaticos. CDN-> Content Delivery Network, Red de Distribución de Contenido. Con esto seguro bajamos mucho la factura en servidores. Seguimos creciendo. Y seguimos a ir muy lento devido al trafico. Toca meter para tareas pesadas las colas de tareas.

![](../doc/img/colas.png)

Son servidores que encolan las tareas y se van ejecutando poco a poco. Esto lo podemos ver cuando compramos las entradas a un concierto, el email de confirmación de compra tarda en llegar porque va a un servidor de cola de tareas. Cobrarlo es al instante. Pero el email que es lento lo manda el servidor de colas de tareas. Ahi se van encolando y mandando en funcion del computo del servidor.

Seguimos creciendo, lo seguimos petando. Esto empieza a ir muy lento. Ahora metemos un servidor de cache.

![](../doc/img/cache.png)

Un servidor de caché es un tipo de servidor que almacena temporalmente datos solicitados desde otros servidores o clientes, con el propósito de mejorar el rendimiento y la velocidad de acceso a esos datos. La caché actúa como una capa intermedia entre el cliente y el servidor de origen.

Cuando un cliente solicita ciertos datos (como una página web, una imagen, un archivo, etc.), el servidor de caché verifica si ya tiene una copia de esos datos almacenada localmente. Si la encuentra, puede entregarlos al cliente de manera rápida, sin necesidad de solicitarlos al servidor de origen nuevamente. Esto ahorra tiempo y ancho de banda, ya que evita la necesidad de transferir los datos a través de la red.

Las caches son dificiles de gestionar. En este mundo hay dos cosas dificiles, nombrar variables y gestionar caches.

Ahora vienen los de financiero y nos dicen que hay que bajar costes, que se gasta mucho. Lo siguiente que podemos hacer es meter un servidor de https de cache:

![](../doc/img/serv.cache.png)

Un servidor de caché sirve para almacenar temporalmente datos solicitados por usuarios, aplicaciones o servicios, con el fin de mejorar la eficiencia y la velocidad de acceso a esos datos. Almacena copias de datos que se acceden con frecuencia para que, en lugar de solicitarlos repetidamente al servidor de origen, puedan entregarse rápidamente desde la caché local. Normalmente se almacena el HTML del recurso requerido.

Nota: el servidor de HTTP suele estar antes del balanceador de carga, la imagen esta mal.

Luego tambien podemos ampliar el balanceador de carga y los DNS Round Robin.

![](../doc/img/dns.png)

El DNS Round Robin es una técnica de distribución de carga utilizada en la resolución de nombres de dominio (DNS) para distribuir el tráfico de red entre varios servidores de manera equitativa. En lugar de asignar una sola dirección IP a un nombre de dominio, se asignan múltiples direcciones IP, cada una correspondiente a un servidor diferente. Cuando se hace una consulta DNS para ese nombre de dominio, el servidor DNS devuelve estas direcciones IP en un orden diferente en cada solicitud, lo que distribuye el tráfico de manera más uniforme entre los servidores.

Esto lo podemos ver si hacemos un ping a google.com desde la consola desde varios ordenadores en distintas partes nos retornara una ip diferente en algunos equipos que hicieron el ping. Ese ip que es diferente es del DNS del balanceador mas cercado del data center de google para que la peticion sea mas rapida.

A continuacion podemos ver e ejemplo de software con que podemos mostar una infraestructura asi:

![](../doc/img/final.png)

A continuación podemos ver un diagrama de arquitectura edX: Plataforma online de cursos: Un sistema bastante complejo.

![](../doc/img/edx.png)

Todo esto que hemos visto tenemos varias plataformas donde las podemos montar tales como: Una plataforma en la nube:

![](../doc/img/donde.png)

Fin del pdf.
