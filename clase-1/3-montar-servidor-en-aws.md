# Montar servidor en AWS:

Vamos a montar nuestro servidor en AWS. Nos loguemos en AWS.

Nos vamos al servicio de EC2.Importante al entrar ver que en la parte superior tengamos seleccionado Norte de virginia, esquina superior derecha. Esto porque es el mas barato de todos.

![](../doc/img/ec2.png)

Pulsamos dentro de Instancias(en ejecución). Vemos un boton en naranja que dice lanzar instancias.

![](../doc/img/instancias.png)

![](../doc/img/instancias1.png)

Le damos a lanzar instancia.

- Nombre y etiqueta: Le ponemos un nombre: web-kc

![](../doc/img/w-1.png)

- Imágenes de aplicaciones y sistema operativo: Seleccionamos ubuntu.

![](../doc/img/w-2.png)

- Tipo de instancia: aqui ya nos viene una que nos dan ellos para la capa gratuita, dejamos esa.

![](../doc/img/w-3.png)

- Par de claves(inicio de sesion): OJO con esto. Le damos donde dice Crear un par nuevo de claves. Esto porque es la primera vez que hacemnos esto, luego ya veremos que si abrimos el desplegable saldran ahi las claves. Esto es entre comillas para el acceso al servidor.

![](../doc/img/w-4.png)

Le ponemos un nombre. Seleccionamos el ED25519 y .pem y luego le damos a crear par de claves.

![](../doc/img/w5.png)

Se nos va a descargar un archivo. Con extension .pem. Lo guardo en la carpeta del modulo. OJO este archivo es importante no perderlo porque con el tendremos acceso al servidor. Ese archivo es la llave del servidor. Tambien podemos crear otro servidor y usar esta misma llave. Ahora vemos que al generar el par de claves se selecionan estas en el desplegable.

![](../doc/img/w6.png)

- Configuracion de la red: Esto es como el cortafuegos, lo dejamos asi por defecto como esta ahi.

![](../doc/img/w7.png)

- Configuración almacenamiento: Lo dejamos tal como esta.

![](../doc/img/w8.png)

La parte de Detalles avanzados no la tocamos. Ahora en la parte lateral derecha tenemos el resumen de la configuracion:

![](../doc/img/w9.png)

Ahora antes de Lanzar la instancia vemos una parte que dice Revisar comandos.

![](../doc/img/w10.png)

Esto es realmente los comandos que podemos usar para crear el servidor. Le damos a Lanzar instancia. Empieza la creacion y nos muestra este mensaje:

![](../doc/img/w11.png)

Ahora para ver los disco duros, lo tenemos en el panel lateral izquierda, ese disco duro si queremos lo podemos llevar a otra máquina si asi lo necesitamos. Esto podria ser el caso que perdamos la llave podriamos llevar el disco duro a otra máquina.

Ahora si seleccionamos la instancia y vamos al desplegable de estado de instancia ahi podriamos apagar la maquina, vemos las demas opciones.

![](../doc/img/w12.png)

OJO: La opcion de terminar, apaga la maquina y la elimina. Esto lo haremos al final del curso.
