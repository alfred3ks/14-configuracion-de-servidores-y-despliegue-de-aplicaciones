# Desplegar app de node y mongodb:

Vamos a desplegar una aplicacion hecha con node mas potente y que tiene un base de datos en mongodb.

Para esto vamos a usar una plataforma llamada Parse:

https://parseplatform.org/

Esto es una plataforma que nos entrega un backend para despreocuparnos de esto. Esto es una plataforma que compro facebook. Al pasar el tiempo vieron que no era rentable y decidieron echar el cierre.

Eso a los desarrolladores les ha creado un problema, es lo que se llama una deuda tecnica.

Tambien lo que hicieron fue liberar el codigo fuente de parse para que el que quiera se montara sus historias.

Esto es un tipico ejemplo que al usar soluciones de terceros adquirimos una deuda tecnica que puede que algun dia lo podemos pagar.

Lo que vamos es desplegar en nuestro servidor parse.

https://github.com/parse-community/parse-server-example

Es un backend de parse hecho con express. Esto es como el sparres que ya usamos pero mucho mejor montado.

Este backend usa la base de datos mongodb. Lo que haremos es instalar mongodb en nuestro servidor.

## 📌 Instalación de MongoDB en nuestro servidor:

Vamos a la pagina oficial de mongodb y leemos la documentacion:

https://www.mongodb.com/es

https://www.mongodb.com/docs/manual/

Vamos a trabajar con la version mas actual que es la 7.

https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/#std-label-install-mdb-community-ubuntu

Tenemos varias maneras de hacerlo. Una es descargar el archivo .tgz ya complilada, y tendria que instalarlo como un servicio de sistema y tendriamos que usar tamien PM2. Lo que haremos es instalarlo usando la guis de instalacion que ellos nos dan.

Aqui resaltamos el tema de la documentacion lo importante que es, debemos instalar y hacer funcionar un dependencia que es muy importante para nuestra aplicacion.

## Pasos a realizar: En el usuario ubuntu

- Importar la clave publica usada por el sistema gestor de paquetes, para eso debemos isntalar el paquete gnupg y curl:

```bash
sudo apt install gnupg curl
```

- Instala la clave GPG publica de MongoDB corriendo el siguiente comando: Mongo nos esta dando su clave publica para poder comunicarnos con ellos:

```bash
curl -fsSL https://www.mongodb.org/static/pgp/server-7.0.asc | \
sudo gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg \
   --dearmor
```

Con estos pasos lo que estamos haciendo es ampliar la capacidad de instalacion de paquetes de mi sistema para poder instalar versiones de mongo que seguro no estaran en la tienda cuando usamos apt.

- Creamos un archivo list para mongodb, en la ruta /etc/apt/sources.list.d/mongodb-org-7.0.list

Esto lo hacemos con el siguiente comando:

```bash
echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/7.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-7.0.list
```

Lo comprobamos:

```bash
cat /etc/apt/sources.list.d/mongodb-org-7.0.list
```

- Recargamos la base de datos de los paquetes:

```bash
sudo apt update
```

- Ya podemos instalar la ultima version de mongodb desde el repositorio oficial de mongo:

```bash
sudo apt install -y mongodb-org
```

- Ejecutar MongoDB, arrancar la BD:

Los direcctorios donde va a estar la bd es el directorio /var/lib/mongodb y la de los log en /var/log/mongodb

Para arrancar mongodb:

```bash
sudo systemctl start mongod
```

Comprobamos que esta funcionando:

```bash
sudo systemctl status mongod
```

```bash
ps aux | grep mongo
```

Ya lo tenemos. Ojo con la instalacion relajada, hemos instalado y funciona lo dejamos, mongo viene muy bien en local pero en produccion amigo a veces da problemas. Vamos a hacer unas comprobaciones.

Veamos estos comandos por si nos hace falta:

```bash
sudo systemctl stop mongod
```

```bash
sudo systemctl restart mongod
```

Vamos a arrancar la consola de mongodb:

```bash
mongosh
```

Vemos las bases de datos instaladas:

```bash
show databases
```

Para salir del bash:

```bash
exit
```

## 📌 Instalación de Parse:

Lo hacemos en el usuario apps

```bash
sudo -u apps -i
```

- Descargamos el repo de parse, dentro de una carpeta que llamamos parse por eso lo ves al final, hacerlo con HTTP:

```bash
git clone https://github.com/parse-community/parse-server-example.git parse
```

OJO parse funciona con la version 18 de node debemos instalar esa version y movernos a ella para poder instalar dependecias:

```bash
nvm i 18
```

```bash
node --version
```

Nos metemos dentro y instalamos sus dependecias:

```bash
cd parse
```

```bash
npm i
```

Ejcutamos para arrancar parse:

```bash
npm start
```

Nos dice en que puerto se esta ejecutando 1337. http://3.91.223.233:1337/

Tenemos que abrir ese puerto. Vamos a AWS y lo abrimos.

![](../doc/img/w32.png)

![](../doc/img/w33.png)

Ya esta lo tenemos ya arrancado. Vamos a ver como funciona parse. Vamos arrancar postman para hacer unas peticiones.

- Creamos un nuevo producto, con una peticion POST cervezas es una prueba:

![](../doc/img/w34.png)

![](../doc/img/w35.png)

Luego hacemos una peticion GET para ver que tenemos:

![](../doc/img/w36.png)

Como vemos tenemos un api rest muy parecido a sparrest. Para cuando hagamos las peticiones tenemos que autenticarnos en las cabeceras como vemos con:

Headers:
X-Parse-Application-Id
myAppId

Ahora como vemos estos datos que hemos guardado se estan guardando en mongodb. Lo comprobamos entramos:

Vamos a arrancar la consola de mongodb:

```bash
mongosh
```

Vemos las bases de datos instaladas:

```bash
show databases
```

Vemos una nueva BD llamada dev:

![](../doc/img/w37.png)

Entramos en esa BD:

```bash
use dev
```

Vemos que el las colecciones tenemos la de cervezas:

```bash
show collections
```

```bash
db.cervezas.find({})
```

![](../doc/img/w38.png)

Estamos guardando todo en mongodb. OJO aqui hay algo que nos debe mosquear que se guarde asi como asi mmmm.

LA base de datos esta abierta, no tiene contraseña. Si cualquiera puede entrar nos podrian colar cualquier cosa o entrar y ver la bd de mongodb, esto es porque tenemos una configuracion relajada de mongodb a eso hacia referencia antes. Asi tenemos un problema de seguridad.

SIEMPRE SIEMPRE SIEMPRE UNA BD DEBE ESTAR PROTEGIDA POR USUARIO Y CONTRASEÑA.

Vamos a proteger la bd, lo vemos en la documentación de mongodb. Nos vamos al apartado de seguridad.

Mongodb nunca viene con seguridad por defecto.

https://mongodb.com/docs/manual/tutorial/configure-scram-client-authentication/

Vamos al apartado que nos dice:

https://www.mongodb.com/docs/manual/tutorial/configure-scram-client-authentication/#create-the-user-administrator

## Crea un usuario administrador:

## Nos cambiamos al usuario admin de mongodb:

```bash
mongosh
```

## Creamos la siguiente instruccion con los datos:

```bash
use admin
db.createUser(
  {
    user: "Your user",
    pwd: "Your password",
    roles: [
      { role: "userAdminAnyDatabase", db: "admin" },
      { role: "readWriteAnyDatabase", db: "admin" }
    ]
  }
)
```

Lo ejecutamos y vemos que se ha creado el usuario con su contraseña. Porque la consola nos ha devuelto un {ok:1}

Lo siguiente es agregar en la configuracion de mongo db que arranque siempre pidiendo las credenciales. Eso lo debemos hacer en un archivo de configuracion de mondodb que tenemos en la ruta etc/mongod.conf

```bash
sudo nano /etc/mongod.conf
```

Vemos que el apartado de segurity esta comentado. #segurity, tenemos que poner como nos dice la documentacion de mongodb:

security:
authorization: enabled

OJO: Cuando metamos esta inforamcion OJo con el tabulado mira como va todo el archivo y tabula este igual si no da fallo.

Guardamos el archivo. Ahora reiniciamos el servicio de mongodb: NO TIENE reload

```bash
sudo systemctl restart mongod
```

```bash
sudo systemctl status mongod
```

Vamos a conectarnos a mongodb usando las credenciales: OJO si hacemos mongosh vemos que se conecta y nos muestra la bd test pero luego le hacemos un show databases y ahi nos dice que debemos estar autenticado. Cosas de mongo.

Vamos a conectarnos apenas entremos no ya dentro y eso lo haremos con el comando:

```bash
mongosh --authenticationDatabase \
    "admin" -u "batman" -p
```

Accedemos sin problemas a las bd.

Ahora vamos al usuario apps y tratamos de arrancar la aplicacion parce y vemos que falla. Eso es porque la bd ya esta con autenticacion.

<span style='color:crimson'>
NOTA: Cada aplicacion que pongamos en el servidor y use mongodb debe tener su propia bd y su propio usuario. NUNCA EL USUARIO DEBE SER EL ADMINISTRADOR porque con el pueden acceder a todas las BD del gestor.
</span>

## 📌 Crear BD para parse y ahi crearemos el usuario para esa BD:

Vemos al documentacion de mongodb:

https://www.mongodb.com/docs/manual/core/security-users/

https://www.mongodb.com/docs/manual/tutorial/create-users/

Primero nos conectamos como el usario adminitrador a las bd.

```bash
mongosh --authenticationDatabase \
    "admin" -u "batman" -p
```

Ya podemos crear el usuario asi:

```bash
use parsedb
db.createUser(
  {
    user: "aqui el usuario",
    pwd: "aqui el password",
    roles: [ { role: "readWrite", db: "aqui pones el nombre de la bd" } ]
  }
)
```

use nodepopdb
db.createUser(
{
user: "tiger22",
pwd: "129622T",
roles: [ { role: "readWrite", db: "nodepopdb" } ]
}
)

Vamos al shell de mongodb nos autenticamos como administrador y ejecutamos la instruccion anterior. Vemos q al crear el usuario nos dice por consola {ok:1}.

Probamos el usuario y la contraseña a ver si esta ok, tratamos de acceder a la bd parsedb con el usuario.

```bash
mongosh -u "pon aqui el usuario" --authenticationDatabase "parsedb" -p
```

Ya hemos probado la autenticacion ahora hay que probar que podamos hacer cosas como escribir, etc.

Ahora debemos darle esas credenciales a parse para que se conecte a la bd. Eso lo vemos en la documentación de parse. Vamos al repo. y viendo el packege.json ahi vemos cual es el archivo de arranque de la aplicacion, es index.js abrimos ese archivo y vemos que la configuracion para conectarse a mongo db lo hace a traves de una variable de entorno.

## Configuracion de la aplicacion index.js:

```javascript
export const config = {
  databaseURI:
    process.env.DATABASE_URI ||
    process.env.MONGODB_URI ||
    'mongodb://localhost:27017/dev',
  cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
  appId: process.env.APP_ID || 'myAppId',
  masterKey: process.env.MASTER_KEY || '', //Add your master key here. Keep it secret!
  serverURL: process.env.SERVER_URL || 'http://localhost:1337/parse', // Don't forget to change to https if needed
  liveQuery: {
    classNames: ['Posts', 'Comments'], // List of classes to support for query subscriptions
  },
};
```

Vamos a agregar las credenciales a la variables de entornos.

Vamos al usuario apps en el servidor. Y entramos dentro de parse. Y usar la version correcta para la aplicacion que fue la version 18 ojo con esto.

## Para ver las variables de entorno en mi servidor:

```bash
env
```

Asi definimos una variable de entorno en nuestro servidor de la siguiente manera, esta forma solo define la varaible durante la sesion si cierro el usuario se perdera:

```bash
export DATABASE_URI=mongodb://<user>:<password>@127.0.0.1:27017/<nombre-bd>
```

Lo comprobamos y vemos que ya aparece en las variables de entorno. Si todo va bien ya podemos arrancar nuestra app de parse y deberia arrancar sin problemas. Si nos vamos al navegador vemos que ahora si carga, ademoa podemos crear una coleccion usando postman. Como hicimos con cervesas pero esa coleccion esta en la bd dev.

## 🎯🎯🎯 ----- Videos 5 -------- Refactoring -----------🎯🎯🎯

Ir al video 5 donde configura correctamente PM2 y supervisor:

## Instalación de PM2: Paso 2: Descanso:

Vamos a usar el gestor de procesos para que se arranque la app por si sola.

Lectura recomendable:

https://12factor.net/es/

https://www.redhat.com/architect/12-factor-app

Vamos a instalar PM2: Vemos la documentacion:

https://pm2.keymetrics.io/docs/usage/quick-start/

https://pm2.keymetrics.io/docs/usage/application-declaration/

Vamos a generar un archivo de configuracion. Lo que vamos a hacer es meter este archivo para el chat que tenemos corriendo con PM2.

## 🎯🎯🎯Configuracion del fichero de configuracion en el node-chat 🎯🎯🎯:

En el home del usuario apps ejecutamos:

```bash
pm2 init simple
```

Esto nos genera un archivo ecosystem.config.js en el home del usuario apps.
Actualmente tenemos arrancado el chat vamos a parar el chat y eliminarlo de la lista de procesos de PM2

```bash
pm2 stop app
```

```bash
pm2 delete app
```

Procedemos a abrir el archivo ecosystem.config.js con nano:

```bash
nano ecosystem.config.js
```

```json
module.exports = {
  apps : [
      {
        name   : "chat",
        cwd:"/home/apps/node-chat",
        script : "app.js"
      }
   ]
}
```

Ejecutamos:

```bash
pm2 start ecosystem.config.js
```

Revisamos el chat y funciona perfectamente.

🎯🎯🎯🎯🎯🎯🎯🎯

## Configuracion del fichero ecosystem.config.js para la aplicacion parse:

Volvemos a edita el archivo ecosystem.config.js:

```bash
nano ecosystem.config.js
```

```json
module.exports = {
  apps : [
      {
        name:"chat",
        cwd:"/home/apps/node-chat",
        script:"app.js"
      },
      {
        name:"parse",
        cwd:"/home/apps/parse",
        script:"index.js"
      }
   ]
}
```

```bash
pm2 list
```

Nos dice que esta arrancada pero al ir a la web no funciona. Osea que no esta onlie como nos dice PM2. Tenemos que ver los log para saber que pasa.

```bash
pm2 logs parse
```

```bash
[TAILING] Tailing last 15 lines for [parse] process (change the value with --lines option)
/home/apps/.pm2/logs/parse-out.log last 15 lines:
1|parse    | warn: DeprecationWarning: The Parse Server option 'allowClientClassCreation' default will change to 'false' in a future version.
1|parse    | warn: DeprecationWarning: The Parse Server option 'allowExpiredAuthDataToken' default will change to 'false' in a future version.
1|parse    | warn: DeprecationWarning: The Parse Server option 'encodeParseObjectInCloudFunction' default will change to 'true' in a future version.
1|parse    | error: Received unauthorized error {"error":{"code":13,"codeName":"Unauthorized","ok":0}}

/home/apps/.pm2/logs/parse-error.log last 15 lines:
1|parse    |     at Connection.onMessage (/home/apps/parse/node_modules/parse-server/node_modules/mongodb/lib/cmap/connection.js:207:30)
1|parse    |     at MessageStream.<anonymous> (/home/apps/parse/node_modules/parse-server/node_modules/mongodb/lib/cmap/connection.js:60:60)
1|parse    |     at MessageStream.emit (node:events:513:28)
1|parse    |     at processIncomingData (/home/apps/parse/node_modules/parse-server/node_modules/mongodb/lib/cmap/message_stream.js:132:20)
1|parse    |     at MessageStream._write (/home/apps/parse/node_modules/parse-server/node_modules/mongodb/lib/cmap/message_stream.js:33:9)
1|parse    |     at writeOrBuffer (node:internal/streams/writable:391:12)
1|parse    |     at _write (node:internal/streams/writable:332:10)
1|parse    |     at MessageStream.Writable.write (node:internal/streams/writable:336:10)
1|parse    |     at Socket.ondata (node:internal/streams/readable:754:22)
1|parse    |     at Socket.emit (node:events:513:28) {
1|parse    |   ok: 0,
1|parse    |   code: 13,
1|parse    |   codeName: 'Unauthorized',
1|parse    |   [Symbol(errorLabels)]: Set(0) {}
1|parse    | }
```

Vemos que el fallo que esta teniendo es que no puede acceder a la bd de mongodb y es normal porque no tenemos las variables de entorno. Le tenemos que decir a PM2 que cree la variable de entorno para que s epueda conectar. Eso lo hacemos en el archivo de configuracion.

```bash
nano ecosystem.config.js
```

```json
module.exports = {
  apps : [
      {
        name:"chat",
        cwd:"/home/apps/node-chat",
        script:"app.js"
      },
      {
        name:"parse",
        cwd:"/home/apps/parse",
        script:"index.js",
        env:{"DATABASE_URI": "mongodb://parseusr:1296@22T@127.0.0.1:27017/parsedb"}
      }
   ]
}
```

Ejecutamos:

```bash
pm2 start ecosystem.config.js
```

Revisamos la web y sigue fallando, no va volvemos a los log:

```bash
pm2 logs parse
```

OJITO, la version de node en ambas app son distintas, esta ejecutando parse con la version 16 y necesitamos es la 18. Debemos a traves de la confguracion el tipo de node que usara. Con la instruccion interprete y la ruta del ejecutable de node se lo diriamos:

Para saber donde estan los ejecutables:

```bash
which node
```

```bash
/home/apps/.nvm/versions/node/v18.19.1/bin/node
/home/apps/.nvm/versions/node/v16.20.2/bin/node
```

```json
module.exports = {
  apps : [
      {
        name:"chat",
        cwd:"/home/apps/node-chat",
        script:"app.js"
      },
      {
        name:"parse",
        cwd:"/home/apps/parse",
        script:"index.js",
        env:{"DATABASE_URI": "mongodb://parseusr:1296@22T@127.0.0.1:27017/parsedb"},
        interpreter:"/home/apps/.nvm/versions/node/v18.19.1/bin/node"
      }
   ]
}
```

Sigue igual fallando. Tenemos un fallo PM2 esta instalado con node 16 necesitamos instalarlo en node 18, asi que debemos reinstalar PM. OJO en el archivo donde configuramos PM2

Este fichero ecosystem esta perfecto. Vamos a instalar la ultima version de node y luego ahi vamos a instalar PM2 como servicio del sistema.

## 🎯🎯🎯** Reinstalacion de PM2 en la ultima version de node: **🎯🎯🎯

```bash
nvm i node
```

Establecemos la version por defecto de node ojo con esto muy necesario:

```bash
nvm alias default 21
```

Vamos a instalar PM2 Lo hacemos de manera global en el usuario apps:

```bash
npm install pm2@latest -g
```

```bash
pm2 --version
```

Buscamos el comando de PM2 que nos dara

```bash
pm2 startup
```

To setup the Startup Script, copy/paste the following command:
Debemos ejecutar este comando. Para instalarlo como servicio del sistema:

```bash
sudo env PATH=$PATH:/home/apps/.nvm/versions/node/v21.6.2/bin /home/apps/.nvm/versions/node/v21.6.2/lib/node_modules/pm2/bin/pm2 startup systemd -u apps --hp /home/apps
```

Aqui le decimos que PM2 se va a ejecutar en la version 21 de node.

Como vemos el comando que nos da va con sudo y con el usuario apps no puedo ejecutar comandos con sudo por lo tanto este comando lo debo ejecutar con el usuario ubuntu que si puede ejecutar comando con sudo.

Nos pasamos al usuario ubuntu y lo ejecutamos. Todo correcto. ahora si reiniciamos el servidor este deberia ejecutarse al arrancar el servidor. Probamos el chat y funciona, pm2 se ha arrancado.

Para asegurarnos vamos a reiniciar el sistema. El servidor. esto para matar el proceso anterior que tenemos mal instalado.

```bash
sudo reboot
```

Comprobamos que esta corriendo:

```bash
sudo systemctl status pm2-apps
```

🎯🎯🎯 FIN DE PM2---🎯🎯🎯🎯

Nos pasamos al usuario apps:

```bash
sudo -u apps -i
```

Verificamos la version de node: Debe ser la 21

```bash
node --version
```

Arrancamos el archivo ecosystem.config

```bash
pm2 start ecosystem.config.js
```

Volvemos a comprobar a ver si parse funciona y no funciona... MONGODB NO funciona porque hemos reiniciado...Mongo por defecto no se aranca con reinio del sistema.

Para esto con el usuario ubuntu ejecutamos: Asi siempre se arrancara al reiniciar el sistema:

## MONGO ARRANCA SIEMPRE AL REINICIAR:

```bash
sudo systemctl enable mongod
```

Respuesta de la consola:

Created symlink /etc/systemd/system/multi-user.target.wants/mongod.service → /lib/systemd/system/mongod.service.

Arrancamos mongo que ahora esta parado:

```bash
sudo systemctl start mongod
```

Comprobamos que este arrancado:

```bash
sudo systemctl status mongod
```

Volvemos al usuario apps y arrancamos el archivo ecosystem.config:

```bash
pm2 start ecosystem.config.js
```

```bash
pm2 restart parse
```

Sigue sin funcionar... vuelta a los logs. NADA NO FUNCIONA!!! Usaremos super visor!!!

```bash
pm2 stop all
```

```bash
pm2 delete all
```

Asi garantizamos que pm2 no vuelva arrancar

```bash
pm2 save --force
```

## 🎯🎯🎯🎯🎯Instalacion de Supervisor:🎯🎯🎯🎯🎯🎯

Volvemos a ubuntu:

```bash
sudo apt install supervisor
```

Ya el solo se registra como servicio del sistema.

```bash
sudo systemctl status supervisor
```

Vamos a configurar supervisor: Nos movemos a la carpeta /etc/supervisor/conf.d/ Esto lo tenemos en los pdf de node:

```bash
cd /etc/supervisor/conf.d/
```

```bash
sudo nano parse.conf
```

Le ponemos esta configuración:

```bash
[program:parse]
user=apps
command=npm start
directory=/home/apps/parse
autostart=true
autorestart=true
environment=DATABASE_URI="mongodb://parseusr:1296@22T@127.0.0.1:27017/parsedb",PATH="/home/apps/.nvm/versions/node/v18.19.0/bin:/usr/bin
```

```bash
sudo systemctl restart supervisor
```

```bash
sudo systemctl status supervisor
```
