# Instalando y desinstalando software:

Hoy en dia todas las distribuciones de linux tiene un gestor de paquetes. En las distribuciones Debian (como ubuntu) se puede instalar y desintalar sotfware a traves del gestor de paquetes:

```bash
apt-get
```

Este gestor se descarga paquetes ya compilados desde internet para tu distribucion y los sistala en el sistema (descargando tambien otras dependencias si hace falta).

## Para instalar paquetes:

```bash
sudo apt install <package>
```

## Para desinstalar paquetes:

```bash
sudo apt purge <package>
```

## Para actualizar repositorios: actualiza los repositorios de software para buscar actualizaciones:

```bash
sudo apt update
```

## Para instalar las actualizaciones en nuestro sistema:

```bash
sudo apt upgrade
```
