# Usuarios, grupos y permisos:

## Usuarios y grupos:

- Linux es un sistema multiusuario.
- Los usuarios no sólo son personas, también pueden ser servicios (como el servidor web, servidor de correo, etc.).
- Las cuentas de usuario, pueden pertenecer a uno o varios grupos.
- Hay unos grupos especiales de administrador (admin o sudo).
- Suele haber una cuenta de super-administrador: root.
- En Ubuntu, el login con root está deshabilitado por seguridad.

## 📌 Permisos y carpetas:

Cada archivo o carpeta pertenece a un usuario y un grupo.

- Los permisos de Linux permiten definir tres tipos de acceso:

  - Lectura.
  - Escritura y/o borrado.
  - Ejecución (para archivos o scripts ejecutables).

- Estos tres tipos de acceso, se definen en tres niveles:

  - Propietario del archivo o carpeta.
  - Usuarios del mismo grupo del archivo o carpeta.
  - Otros usuarios fuera del grupo.

![](../doc/img/permisos.png)

![](../doc/img/permisos1.png)

## 📌 Modificar permisos de archivos y carpetas:

```bash
chmod <target>±rwx <filename>
```

```bash
<target> = a / u / g / o

a = all
u = user/owner
g = group
o = other
```

## Ejemplo:

```bash
chmod u+wrx backup
Da (+) todos los (rwx) los permisos para el propietarios u.

chmod a+x backup
Da (+) permisos de ejecución (x) para todos los (a)

chmod g+rx backup
Da (+) permisos de lectura y ejecución (rx) a los usuarios del grupo (g)

chmod o-rw backup
Quita (-) permisos de lectura y escritura (rw) a los otros usuarios (o)
```

## 📌 Ejecutar como administrador: Usamos el comando sudo

```bash
sudo <comand>
```

Ejecuta lo que viene seguido de sudo como administrador.
El usuario debe tener permiso de sudoer.

## 📌 Crear usuario:

```bash
sudo adduser <username>
```

Crear un usuario y un grupo con el mismo nombre del usuario y mete al usuario en dicho grupo.
Esto lo registra en /etc/passwd y /etc/group.

Si tratamos de crear un usuario vemos que nos dice que solo el usuario root puede crear usuarios, pero como tambien hemos visto el usuario root esta desabilitado, para eso nos vamos a convertir en usuario root de manera momentanea.

## 📌 Eliminar usuario:

```bash
sudo deluser <username>
```

Elimina un usuario del sistema.
Lo elimina de /etc/passwd

## 📌 Crear grupo:

```bash
sudo addgroup <groupname>
```

Crea un grupo añadiéndolo a /etc/group

## 📌 Eliminar grupo:

```bash
sudo delgroup <groupname>
```

Elimina un grupo de /etc/group
No elimina los usuarios de ese grupo.

## 📌 Añadir un usuario a un grupo:

```bash
sudo adduser <username> <groupname>
```

Añade el usuario al grupo en /etc/group

## 📌 Permitir a un usuario hacer sudo:

```bash
sudo adduser <username> sudo
```

Añade el usuario al grupo sudo en /etc/groupSi eres sudo, tienes permisos para todo, da igual los permisos del archivo.

## 📌 Creamos el uuario goku:

![](../doc/img/user.png)

## 📌 Ejercicio:

Vamos a crear dos usuarios, a goku y vegeta, usaremos la instruccion:

```bash
sudo adduser goku
```

```bash
sudo adduser vegeta
```

Al darle a crear el usuario vemos que nos pide que metamos una contraseña, y ademas nos pide rellenar un formulario, con darle enter suficiente. Esta configuracion de estos usuarios se guardan el la siguiente ruta /etc/passwd

```bash
cat /etc/passwd
```

![](../doc/img/user1.png)

Si editamos este archivo, por ejemplo eliminamos a goku o vegeta, eliminamos a estos usuarios.Ademas si vemos en esta archivo que hay un monton de mas usuarios, no solo son personas tambien los programas son usuarios. Vamos a desgranar un poco el suarios:

```bash
vegeta:x:1002:1002:,,,:/home/vegeta:/bin/bash
```

Los dos puntos : es el separador.
x: aqui iria la contraseña,
1002: el id del usuario,
1002: el id del grupo al que pertenece este usuario por defecto,
,,,: Aqui iria la información que metemos en el formulario al crear el usuario, como no metimas nada sale asi,
/home/vegeta: esto es la ruta del home del usuario,
/bin/bash: La ruta de la términal por defecto que usa este usuario.

Para ver la información de los grupos lo tenemos en la siguiente ruta etc/group.

```bash
cat /etc/group
```

![](../doc/img/group.png)

Aqui tambien podemos ver el usuario ubuntu pertenece al grupo sudo, por eso puede ejecutar el comando sudo:

![](../doc/img/sudo.png)

Tenemos otro fichero el etc/shadow, es donde tenemos las contraseñas encriptadas de los usuarios.

```bash
sudo cat /etc/shadow
```

![](../doc/img/shadow.png)

## 📌 Conexion al servidor con los usuarios creados: Modificar fichero ssh del servidor:

Aqui debemos que tener en cuenta unas cosas, para estos usuarios creados no tenemos llave, la llave creada es para el usuario ubuntu, el archivo web-kc.pem.

**<span style="color:crimson;">OJO:</span> Por defecto en AWS viene desactivado el login con contraseña. Solo se puede hacer login con certificado.**

Lo que haremos es cambiar en AWS que podamos acceder por contraseña. Esto lo podemos ver en la siguiente ruta /etc/ssh/

```bash
cd /etc/ssh/
```

![](../doc/img/w13.png)

De aqui el archivo que nos interesa es: -rw-r--r-- 1 root root 3252 Dec 7 02:12 sshd_config, este es el archivo que tiene la consfuracion del servidor ssh de AWS.

Sabemos que es este por la letra d que lleva la final, en linux las aplicaciones que estan siempre corriendo se les llaman demonios. OJO OJO al editar el archivo sshd_config si la liamos ya no nos podremos conectar el servidor. Para eso podemos hacer una copia del mismo. Vamos a realizar una copia:

```bash
sudo cp sshd_config sshd_config.backup
```

![](../doc/img/w14.png)

Ahora ya podemos modificar, vamos a editarlo con nano.

```bash
sudo nano sshd_config
```

Cambiamos esta linea:

```bash
PasswordAuthentication yes
```

Para guardar en nano y salir.

ctrl + x
yes
enter

Ahora debemos con el controlador del sistema saber el estado del servidor ssh:

```bash
sudo systemctl status ssh
```

Algunos servicios no todos nos permiten hacer un <reload> del servicio, ojo para reiniciar tendriamos que usar <restart> pero si la hemos liado en el archivo de configuracion ya no podriamos entrar.

NO USAR ESTE: OJO CON ESTE:

```bash
sudo systemctl restart ssh
```

Esto sin parar el servicio va al archivo de configuracion y intenta de cargarlo pero sin afectar el servicio. Si hay un error en el archivo de configuracion daria el error pero se queda con la configurcion antigua.

```bash
sudo systemctl reload ssh
```

Probamos conectarnos: Nos pedira la contraseña:

```bash
ssh goku@3.92.88.128
```

```bash
ssh vegeta@3.92.88.128
```

Vemos como se conecta perfectamente. Conseguido.

## Comandos utiles: OJO UN Tips!!! OJO a los permisos.

Desde Vegeta hacemos esto al archivo que esta en goku.

```bash
echo "Mi nombre es Alfredo" > hello
```

Con este comando podemos desde el usuario vegeta meter el contenido Mi nombre es Alfredo en el archivo hello. Esto puede ser muy interesante si solo tenemos permisos de escritura y no de lectura del archivo hello que esta en goku.

```bash
echo "Soy developer" >> hello
```

Si usamos el doble >> mantenemos el contenido que existe en el archivo hello agregando la nueva linea, si usamos un solo > solo mete el contenido que le hemos pasado nosotros.

## 📌 Jugando con Grupos:

Solo pueden crear grupos los usuarios que son administradores y ese es ubuntu.

```bash
sudo addgroup saiyans
```

Ahora vamos a añadir a goku y vegeta a este grupo:

```bash
sudo adduser goku saiyans
```

```bash
sudo adduser vegeta saiyans
```

NOTA: Para que se apliquen los cambios y veamos a que grupos pertenece un usuario debemos deslogueanos y volvernos a loguear.

Para saber a que grupo pertence un usuario:

```bash
groups
```

Ahora vemos que el archivo hello que esta en goku pertenece a goku y al grupo por defecto goku, loque haremos es hace que este en el grupo sainyans. Esto es que al peretencer al mismo grupo el archivo podria ser editado por ambos usuarios del grupo.

## 📌 Cambiar de propietario de un archivo o directorio: Ver pdf:

Solo lo puede hacer el administrador con sudo.

```bash
sudo chown <newowner> <filename>
```

El archivo <filename> pasa a pertenecer a <newowner>

```bash
sudo chown -R <newowner> <foldername>
```

Cambiar el propietario de <foldername> y todos sus archivos y subdirectorios.
Es un cambio recursivo (-R).

## 📌 Cambiar el grupo de un archivo o directorio: Ver pdf:

```bash
sudo chgrp <newgroup> <filename>
```

El archivo <filename> pasa a pertenecer a <newgroup>

```bash
sudo chgrp -R <newgroup> <foldername>
```

Cambiar el grupo de <foldername> y todos sus archivos y subdirectorios.
Es un cambio recursivo (-R).

Para saber a que grupo pertence un usuario:

```bash
groups
```

Vamos a cambiar el archivo hello desde goku al grupo saiyans

```bash
chgrp saiyans hello
```

![](../doc/img/w15.png)

Ahora vegeta ya tiene acceso igual que goku al archivo. Ya vemos la importancia de los grupos. No tiene mas historia.

## 📌 Como cambiar el password de un usuario desde el propio usuario:

```bash
passwd
```

Nos pedira la actual contraseña y luego la nueva contraseña.

## 📌 Como cambiar el password de un usuario desde el usuario administrador:

```bash
sudo passwd goku
```

Nos pedira la nueva contraseña. No la antigua, esto es cuando se ha perdido la contraseña y no se puede entrar.

## 📌 Apagar y reiniciar el servidor: solo lo puede hacer el administrador

```bash
sudo shutdown
```

```bash
sudo reboot
```
