# NGINX:

Vamos a aprender a montar nuestro primer servidor web. El todo en uno, un servidor web, un proxy inverso y un valanceador de carga. aqui aprenderemos a usarlo como servidor web y como proxy inverso.

Historia de NGIX:

Creado en 2002 por Igor Sysoev.

- Nace como respuesta al problema C10K: manejar 10.000 conexiones simultáneas.
- Muy bajo consumo de recursos (frente a Apache).
- Fácil de configurar (frente a Apache).
- No procesa archivos dinámicos (como PHP o Python), delega esta tarea en servidores de aplicación y actúa como proxy inverso.

## Instalando nginx:

```bash
sudo apt install nginx
```

Al tratarse de un programa servidor debe estar arrancado todo el tiempo. Asi podremos servir paginas web. Vamos a ver como esta el servido, asi vemos si esta arrancado o no, tenemos dos opciones:

```bash
sudo systemctl status nginx
```

```bash
ps aux | grep nginx
```

Vamos a verlo ahora por medio del navegador, ponemos la ip que nos da AWS. Pero antes tenemos que abrir el puerto en AWS. Para el tema de web tenemos el puerto 80. Eso lo tenemos que cambiar. Vamos al panel de AWS seleccionamos el servidor y buscamos en la pestaña Security y hacemos click donde dice grupos de seguridad, nos mandara a otra pagina. alli ya podremos editar las reglas del cortafuego de AWS.

![](../doc/img/w16.png)

Como vemos en las reglas del cortafuego solo esta permitido el puerto 22 del ssh.

![](../doc/img/w17.png)

Lo que haremos ahora es dar click al boton de editar las reglas de estrada. se nos abre otra pagina donde podemos generar una nueva regla. Buscamos el tipo que sea http y luego en la opcion de origen tenemos que selecciona anywhere 0.0.0.0./0, esto quiere decir que desde cualquier ip se puede acceder al servidor, luego le damos a salvar las reglas.

![](../doc/img/w18.png)

Ahora si por el navegador buscamos por la ip del servidor ya vemos que podemos acceder: Ya lo tenemos.

![](../doc/img/w19.png)

## Como parar nginx:

```bash
sudo systemctl stop nginx
```

## Como arrancar nginx:

```bash
sudo systemctl start nginx
```

## Como reiniciar nginx: NO RECOMENDADO

```bash
sudo systemctl restart nginx
```

## Como recargar nginx: RECOMENDADO

```bash
sudo systemctl reload nginx
```

## Como probar la configuracion de NGINX

Muy util cuando modificamos su configuración, hace una compobacion antes de arrancarlo.

```bash
sudo  nginx -t
```

## 📌 CONFIGURACIÓN DE NGINX:

Recordar esa ruta es donde siempre en linux esta la configuración de las aplicaciones que instalamos:

- El fichero de configuración esta en /etc/nginx/nginx.conf
- En /etc/nginx/conf.d/ podemos incluir configuración personalizada que sobre escriba parametros por defecto(si no queremos tocar /etc/nginx/nginx.conf)

Lo vemos por la consola de comandos. Si hacemos un less /etc/nginx/nginx.conf esto es lo que tenemos: con less podemos ver el archivo poco a poco con la consola:

```bash
less /etc/nginx/nginx.conf
```

```bash
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
        worker_connections 768;
        # multi_accept on;
}

http {

        ##
        # Basic Settings
        ##

        sendfile on;
        tcp_nopush on;
        types_hash_max_size 2048;
        # server_tokens off;

        # server_names_hash_bucket_size 64;
        # server_name_in_redirect off;

        include /etc/nginx/mime.types;
        default_type application/octet-stream;

        ##
        # SSL Settings
        ##

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;

        ##
        # Logging Settings
        ##

        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;

        ##
        # Gzip Settings
        ##

        gzip on;

        # gzip_vary on;
        # gzip_proxied any;
        # gzip_comp_level 6;
        # gzip_buffers 16 8k;
        # gzip_http_version 1.1;
        # gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

        ##
        # Virtual Host Configs
        ##

        include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/sites-enabled/*;
}


#mail {
#       # See sample authentication script at:
#       # http://wiki.nginx.org/ImapAuthenticateWithApachePhpScript
#
#       # auth_http localhost/auth.php;
#       # pop3_capabilities "TOP" "USER";
#       # imap_capabilities "IMAP4rev1" "UIDPLUS";
#
#       server {
#               listen     localhost:110;
#               protocol   pop3;
#               proxy      on;
#       }
#
#       server {
#               listen     localhost:143;
#               protocol   imap;
#               proxy      on;
#       }
#}
```

En el pdf de la clase tenemos la explicación de cada una de las cosas que vemos en el fichero de configuración.

## NOTA:OJO con esto en linux solo root puede arrancar procesos que escuchan por debajo del 1024. Por eso cuando vemos el proceso master esta arrancado por root:

```bash
ps aux | grep nginx
```

```bash
root        1416  0.0  1.2  55188 12032 ?        S    17:47   0:00 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
www-data    1419  0.0  0.6  55860  5984 ?        S    17:47   0:00 nginx: worker process
ubuntu      1614  0.0  0.2   7008  2304 pts/0    S+   18:43   0:00 grep --color=auto nginx
```

## Configuracion de nuestros sitios web:

De este archivo tenemos esta parte de la configuración que sera donde pondremos la configuracion delas paginas web que vamos a mostrar:

```bash
include /etc/nginx/sites-enabled/*;
```

- Con una sola instalación, podemos dar servicio a varios dominios o subdominios.
- En /etc/nginx/sites-available/ podemos incluir configuración de otros sitios que queramos configurar.
- Una vez configurados, debemos hacer un acceso directo (ln -s) el archivo a /etc/nginx/sites-enabled/ para que nginx los sirva.

Veamos que pinta tiene el archivo de configuracion de un sitio web en nginx:

```bash
server {
   # puerto en el que escucha ipv4, default_server (server por defecto, único)
   listen 80 default_server;
   # puerto en el que escucha ipv6 (sólo puede haber un ipv6only=on)
   listen [::]:80 default_server ipv6only=on;
   # ruta de donde servir los archivos estáticos
   root /usr/share/nginx/html;
   # archivos considerados como índices
   index index.html index.htm;
   # nombre del servidor (dominio o dominios)
   server_name localhost;
   # cuando la ruta empiece por / buscar localmente la ruta tal cual
   # o terminada por /, si no encuentra nada, devolver un 404
   location / {
      try_files $uri $uri/ =404;
   }
}
```

Ahora vamos a descubrir porque cuando navegamos con la ip el servidor nos muestra la página de welcome to nginx!. Vamos por consola a:

```bash
cd /etc/nginx/
```

```bash
ls -l
```

![](../doc/img/w20.png)

Nos vamos a la carpeta sites-enabled/

```bash
cd /sites-enabled/
```

Vemos que tenemos un archivo llamado default que es un acceso directo a /etc/ngnix/site-avaliable/default

![](../doc/img/w21.png)

Podemos ver ese archivo:

```bash
less default
```

```bash
##
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# https://www.nginx.com/resources/wiki/start/
# https://www.nginx.com/resources/wiki/start/topics/tutorials/config_pitfalls/
# https://wiki.debian.org/Nginx/DirectoryStructure
#
# In most cases, administrators will remove this file from sites-enabled/ and
# leave it as reference inside of sites-available where it will continue to be
# updated by the nginx packaging team.
#
# This file will automatically load configuration files provided by other
# applications, such as Drupal or Wordpress. These applications will be made
# available underneath a path with that package name, such as /drupal8.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

# Default server configuration
#
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        # SSL configuration
        #
        # listen 443 ssl default_server;
        # listen [::]:443 ssl default_server;
        #
        # Note: You should disable gzip for SSL traffic.
        # See: https://bugs.debian.org/773332
        #
        # Read up on ssl_ciphers to ensure a secure configuration.
        # See: https://bugs.debian.org/765782
        #
        # Self signed certs generated by the ssl-cert package
        # Don't use them in a production server!
        #
        # include snippets/snakeoil.conf;

        root /var/www/html;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name _;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri $uri/ =404;
        }

        # pass PHP scripts to FastCGI server
        #
        #location ~ \.php$ {
        #       include snippets/fastcgi-php.conf;
        #
        #       # With php-fpm (or other unix sockets):
        #       fastcgi_pass unix:/run/php/php7.4-fpm.sock;
        #       # With php-cgi (or other tcp sockets):
        #       fastcgi_pass 127.0.0.1:9000;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #       deny all;
        #}
}


# Virtual Host configuration for example.com
#
# You can move that to a different file under sites-available/ and symlink that
# to sites-enabled/ to enable it.
#
#server {
#       listen 80;
#       listen [::]:80;
#
#       server_name example.com;
#
#       root /var/www/example.com;
#       index index.html;
#
#       location / {
#               try_files $uri $uri/ =404;
#       }
#}
```

De aqui nos interesa esta linea dos lineas: Uno es la ruta donde estaran los archivos y la segunda son los tipos de archivos que cargararn siguiendo ese orden, el primero que encuentre sera el que cargara.

```bash
root /var/www/html;
index index.html index.htm index.nginx-debian.html;
```

Vamos por consola a las carpeta html:

```bash
cd /var/www/html
```

Aqui tenemos la web que se esta mostrando:

![](../doc/img/w22.png)

Como vemos si creamos un archivo index.html seria ese primer archivo el que cargara. Lo creamos con nano y refrescamos y vemos que en efecto se carga nuestro archivo:

![](../doc/img/w23.png)

## 📌 Subir una pagiña web a nuestro servidor:

Vamos a buscar en la web en esta página: start bootstrap, vamos a usar un template.

https://startbootstrap.com/
https://startbootstrap.com/theme/personal

Descargamos este: se nos baja en un zip y lo descomprimimos y lo tenemos que llevar al servidor. Vamos a ver como subir y descargar archivos al servidor.

Abrimos una consola donde esta el archivo que deseamos subir.

Para esto tenemos el comando "scp". Nos permite transferir archivos usando el protocolo ssh. Tenemos que poner el origen y el destino.

```bash
scp -r -i web-kc.pem startbootstrap-personal-gh-pages ubuntu@3.80.29.111:/home/ubuntu
```

Vemos como se ha subido nuestra carpeta. Lo comprobamos en el servidor.

![](../doc/img/w24.png)

Si quisieramos descargar seria al reves. OJO los archivos tanto de la credenciales y el archivo estan en la misma carpeta sino tendriamos que poner las rutas. OJO con eso.

Ahora estos archivos los debemos mover a la carpeta /var/www/html.

```bash
sudo cp -rf startbootstrap-personal-gh-pages/* /var/www/html
```

En mi caso lo que hace es copiar toda la carpeta no todos los archivos entonces lo que he hecho es mover los archivos y borrar luego la carpeta.

Muevo los archivos:

```bash
sudo mv /var/www/html/startbootstrap-personal-gh-pages/* /var/www/html/
```

Borro la carpeta:

```bash
sudo rm -rf startbootstrap-personal-gh-pages
```

![](../doc/img/w25.png)

Recargamos y ya tenemos la página funcionando. Ya tenemos nuestra web estatica subida!!!! Esto sera parte de la practica. Subir la aplicacion de react basico.
