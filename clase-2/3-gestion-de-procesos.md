# Gestión de procesos:

En linux hay un comando que es el comando "top".Este comando nos muestra en tiempo real información de los procesos ordenados por consumo de CPU(de mayor a menor).

```bash
top
```

Para salir pulsamos la letra q (quit).

## Para poder ver todos los procesos que se estan ejecutando:

Muestra todos los procesos en ejecución con mucha mas información:

```bash
ps aux
```

Para poder ir viendo poco a poco:

```bash
ps aux | more
```

Para buscar un procesos <command>:

```bash
ps aux | grep <command>
```

Por ejemplo:

```bash
ps aux | grep ssh
```

## Matar un proceso:

Mata el proceso numero <pid>. Solo vale para procesos de este usuario.

```bash
kill <pid>
```

Para poder matar procesos de otros usuarios:

```bash
sudo kill <pid>
```

## Gestionar espacio en disco:

### Espacio libre:

Muestra el espacio libre de las particiones en bytes:

```bash
df
```

Muestra el espacio libre legible para humanos(h):

```bash
df -h
```

### Espacio ocupado:

Muestra lo que ocupa cada archivo dentro del <folder>:

```bash
du <folder>
```

Más rapido (c) y entendible para humanos (h):

```bash
du -ch <folder>
```
