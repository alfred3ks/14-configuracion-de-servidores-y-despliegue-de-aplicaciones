# Desplegar app de React.

## Nginx configuración:

Continuamos con la configuracion de nginx. Podemos ver que con una sola configuracion de nginx podemos dar servicios a todos los dominio que queramos.

Hoy vamos a desplegar una aplicacion de React. La aplicacion que vamos a desplegar es la que tenemos en el siguiente enlace:

https://github.com/mjbuckley/react-redux-todo-app

OJO con esto vamos a poder hacer el ejercicio de la práctica.

Lo primero que debemos hacer es en la aplicacion que nos descargamos generar el build.

Vemos el script para hacer build. Esta es una forma, ahora el nos enseña otra forma, vamos al proyecto y creamos dentro de src un archivo llamado .devcontainer.json y dentro incluimos un objeto asi:

## .devcontainer.json:

```json
{
  "image": "node:latest"
}
```

Han salido los devcontainer. Esto tiene que ver con docker. En lugar de desarrollador en el ordenador y tener que instalar node lo que hacemos es desarrolladar dentro de un contener de docker.

Debemos tener instalada una aplicación que se llama DevContainer de Microsoft. Tambien debemos tener instalado docker en la maquina.

Ahora con ctrl + p buscamos la opcion Reopen in Container y nos abre la aplicacion en un contenedor de docker y ahi ahira ya podemos hacer el npm i y el npm run build. OJO pero debemos tener docker instalado. Asi que lo haremos sin docker.

Ejecutamos las script:

```bash
npm i
```

```bash
npm run build
```

Vemos como nos genera la carpeta build de nuestro proyecto. Estos son los archivos que vamos a subir a nuestro servidor.

NOTA:
Nunca hagas lo que hacen los listillos de bajar la aplicacion directamente en el servidor y luego ahi hacer la build, primero porque tenemos que instalar node y eso consume recursos, moraleja no vayas de listillo y has todo en tu pc y sube solo la build al servidor de produccion.

Vamos a carpeta donde tenemos el proyecto y la llave para el servidor y abrimos una consola.

```bash
scp -r -i web-kc.pem react-redux-todo-app/build ubuntu@107.23.164.100:/home/ubuntu
```

Ya lo tenemos subido al servidor en la carpeta /home/ubuntu.

Ahora vamos a renombrar la carpeta build por un nombre mas caracteristico:

```bash
mv build react-todo
```

Actualmente nuestro servidor cuando buscamos por el navegador con la ip esta mostrando la pantilla de html,css,js que subimos en la clase anterior.

Vamos al panel de AWS y vemos que en la instancia tenemos un ip publica. Que es la que se esta sirviendo la pagina actual.

Tambien vemos que tenemos al lado un opcion que dice DNS si copiamos eso y lo llevamos al navegador nos mostrara la misma pagina, la dns esta redirigiendo a la IP.

![](../doc/img/w26.png)

ip: 107.23.164.100
dns: ec2-107-23-164-100.compute-1.amazonaws.com

Quiero que cuando entre por la ip me cargue la aplicacion de html y por la dns la de react.

Vamos a la carpeta de configuracion de ngnix en el servidor.

```bash
cd /etc/nginx/
```

```bash
ls -l
```

Tenemos estas dos carpetas:

```bash
drwxr-xr-x 2 root root 4096 Feb 19 17:47 sites-available
drwxr-xr-x 2 root root 4096 Feb 19 17:47 sites-enabled
```

El la carpeta sites-available(sitios disponibles) vamos a encontrar los ficheros de configuración de paginas web. Aqui es donde vamos a poner los ficheros de configuracion de nuestra página web.

En la carpeta sites-enabled(sitios activos) es donde vamos a encontrar los ficheros de configuracion de las paginas web que nginx esta sirviendo. Pero aqui hay un truco. Aqui vamos a encontrar es un enlace simbolico, como un link, lo sabemos porque lo dice la primera descripcion del fichero al verlo:

```bash
ls -l sites-enabled/
```

lrwxrwxrwx 1 root root 34 Feb 19 17:47 default -> /etc/nginx/sites-available/default

El archivo de configuracion siempre lo vamos a crear en la carpeta sites-available pero solo vamos a servir las que pongamos en sites-enabled

## Creación del fichero de configuracion dentro de site-available:

```bash
sudo nano react-todo
```

## react-todo:

```json
server {
  listen 80;
  server_name <PON AQUI TU DNS DE AWS>;
  root <TU RUTA A LA APP DE REACT>;
  index index.html;
  location / {
    try_files $uri $uri/ =404;
  }
}
```

```json
server {
  listen 80;
  server_name ec2-107-23-164-100.compute-1.amazonaws.com;
  root /home/ubuntu/react-todo;
  index index.html;
  location / {
    try_files $uri $uri/ =404;
  }
}
```

Ya tenemos el archivo de configuracion ahora vamos a crear el acceso directo en la carpeta sites-enabled, lo podemos hacer estando dentro de /etc/ngnix

```bash
cd sites-enabled
```

Para crear un acceso directo en linux usamos el comando "ln", luego usamos el modificador -s, esto es porque en linux existen los enlaces blandos y los duros. Con el modificador -s estamos creando un enlace blando.
Luego ruta del fichero original y luego la ruta del fichero que sera un link

Para mas información de los enlaces en linux aqui podemos profundizar:

https://medium.com/@307/hard-links-and-symbolic-links-a-comparison-7f2b56864cdd

```bash
sudo ln -s /etc/nginx/sites-available/react-todo /etc/nginx/sites-enabled/react-todo-link
```

Lo comprobamos y vemos que se ha creado.
lrwxrwxrwx 1 root root 37 Feb 20 19:01 react-todo-link -> /etc/nginx/sites-available/react-todo

Ahora vamos a comprobar haciendo un cat del acceso directo vemos el contenido del fichero, asi nos aseguramos que esta todo bien.

```bash
cat sites-enabled/react-todo-link
```

![](../doc/img/w27.png)

Perfecto esto esta bien. Ahora debemos reiniciar nginx:

```bash
sudo systemctl reload nginx
```

Ahora si navegamos a la web usando la dns deberiamos ver la web de react que hemos subido. Y nos sale un 404 del tamaño de una catedral.

Cuando esto nos pase debemos ir a los log, eso lo tenemos en /var/log vamos a ella:

```bash
cd /var/log
```

Aqui vemos que tenemos una carpeta llamada nginx accedemos a ella:

```bash
cd nginx
```

Aqui tenemos estos archivos:

```bash
-rw-r----- 1 www-data adm 44216 Feb 20 19:16 access.log
-rw-r----- 1 www-data adm  5551 Feb 20 19:16 error.log
```

Los vemos con un less para ver poco a poco el fichero o tail para ver el final del fichero:

```bash
tail error.log
```

Vemos que nos dice (Permission denied). Aunq no podemos fiarnos porque imagina la cantidad de log que tenemos. Lo que haremos es usar el comando tail con la bandera -f la cual le hara un seguimiento al log la consola se quedara en espera siguiendo los log. Y vamos y hacemos una peticion a la web y vemos como cargan los log. Vamos a filtrar que sea el log que generamos nosotros. Tenemos que saber cual es nuestra IP. Lo podemos ver con la web what is my ip.

213.37.241.168

Teniendo la IP filtramos los log con esa ip. Asi ya podemos filtrar los fallos que generamos nosotros.

```bash
tail -f error.log | grep 213.37.241.168
```

Ya vemos que el error que tenemos es de permisos. Un permiso denegado es que alguien esta tratando de acceder a un sitio que no puede.

Vamos a ver los permisos que tiene la carpeta que tenemos el proyecto:

```bash
drwxr-xr-x 3 ubuntu ubuntu 4096 Feb 20 17:50 react-todo
```

Por lo que vemos esto no es. tiene permisos de lectura y ejecucion. Pero si miramos la carpeta ubuntu no tiene permisos:

```bash
drwxr-x--- 6 ubuntu ubuntu 4096 Feb 20 17:54 ubuntu
```

Aun asi no estamos seguros quien esta tratando de eacceder a la carpeta ubuntu pero lo podemos ver si vemos los procesos de ngnix

```bash
ps aux | grep nginx
```

```bash
root         431  0.0  0.5  55368  5756 ?        Ss   16:19   0:00 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
www-data    1166  0.0  0.6  56000  6272 ?        S    19:11   0:00 nginx: worker process
ubuntu      1224  0.0  0.2   7008  2304 pts/1    S+   19:46   0:00 grep --color=auto nginx
```

Vemos que el work process es el que esta trtando de acceder. Este es el que lo hace. es el que sirve el fichero. El usuario que ejecuta el worker es www-data y este usuario seguramente no tiene permisos para acceder a la carpeta ubuntu. Como este usuario no es ubuntu ni pertenece al grupo ubuntu entonces no tiene permisos.

Existen varias opciones:

- Cambiar los permisos de la carpeta ubuntu para que cualquier usuario tenga acceso a esta.
- Cambiar le grupo de usuario www-data al grupo de ubuntu.
- Mover los archivos de la aplicacion a la carpeta /var/www

Optaremos por la ultima opcion. OJO tendremos que modificar el fichero de configuracion tambien.

```bash
sudo mv /home/ubuntu/react-todo /var/www/
```

Comprobamos que se ha movido:

```bash
ls -l /var/www
```

Ahora vamos y hacemos la modificacion del archivo de configuracion que tenemos en

```bash
sudo nano /etc/nginx/sites-available/react-todo
```

```bash
server {
  listen 80;
  server_name ec2-107-23-164-100.compute-1.amazonaws.com;
  root /var/www/react-todo;
  index index.html;
  location / {
    try_files $uri $uri/ =404;
  }
}
```

Volvemos a recargar el servicio de NGINX:

```bash
sudo  nginx -t
```

```bash
sudo systemctl reload nginx
```

Bueno ya no vemos el error 404 pero seguimos sin ver la web. Nos sale en blanco. Ya el problema de los permisos solucionado. vemos la consola del navegador y tenemos un error 404. El servior esta tratando de cargar los archivos y no los encuentra. Tenemos un problema de url. Esto es un problema de nuestra aplicacion. No de ngnix. El error lo tenemos cuando hemos hecho el build de la aplicacion en nuestro archivo .package.json tenemos un clave asi:

```json
"homepage": "https://mjbuckley.github.io/react-redux-todo-app",
```

Cuando hacemos la build se nos esta poniendo la url react-redux-todo-app y por eso nos esta fallando. Lo borramos y volvemos hacer la build. luego la volvemos a subir.

Volvemos a abrir desde la terminal donde esta el proyecto y las claves.

```bash
scp -r -i web-kc.pem react-redux-todo-app/build ubuntu@107.23.164.100:/home/ubuntu
```

Ya lo tenemos subido al servidor en la carpeta /home/ubuntu.

Ahora vamos a renombrar la carpeta build por un nombre mas caracteristico:

```bash
mv build react-todo
```

Ahora borramos el proyecto viejo e¡que tenemos subida dentro de /var/www

```bash
sudo rm -rf /var/www/react-todo
```

Procedemos a mover la build subida a las carpeta /var/www:

```bash
sudo mv /home/ubuntu/react-todo /var/www/
```

Ahora si recargamos la web vemos que ahora funciona!!!!

📌📌📌
<spam style='color:crimson'>
OJO: Motivo de practica NO APTA!!! Tema react router!!!
</spam>

No tenemos aun correctamente configurado NGINX para aceptar las rutas cuando le decimos que cargue carga un index o luego un index.html pero si le decimos por ejemplo la ruta http://ec2-107-23-164-100.compute-1.amazonaws.com/SHOW_ACTIVE desde otra pesaña nos da un 404 y eso es porque en nuestro proyecto no hay ningun archivo con esa ruta. Esto es porque react es una single page app.

Vamos a modificar el archivo de configuracion de ngnix: Hacemos la modificacion en el apartado location:

```bash
sudo nano /etc/nginx/sites-available/react-todo
```

```bash
server {
  listen 80;
  server_name ec2-107-23-164-100.compute-1.amazonaws.com;
  root /var/www/react-todo;
  index index.html;
  location / {
    try_files $uri $uri/ /index.html;
  }
}
```

Volvemos a recargar el servicio de NGINX:

```bash
sudo  nginx -t
```

```bash
sudo systemctl reload nginx
```

Recargamos la página la ruta http://ec2-107-23-164-100.compute-1.amazonaws.com/SHOW_ACTIVE y funciona!!!!
📌📌📌
