# 📌 Desplegar aplicación de node: Primer paso:

Vamos a desplegar 2 aplicaciones en esta parte, una que no tiene BD que sera un chat y otra que si tiene una BD de mongodb.

- Node funciona tal cual desarrollamos en producción.
- Lo que debemos hacer es que arranque cuando el servidor se inicie(por si hubiera un reinicio del servidor inesperado).
- Para ello, podemos usar supervisor pm2: un gestor de procesos para apps de Node.
- Reinicia las apps si se caen o son matadas.
- Es mas sencillo que crear un script para r.c o systemd.

Los procesos en linux corren con un usuario, nginx corre con un usuario que es www-data, nuestra aplicación en el servidor va a ejecutarse con un usuario. Debemos crear un usuario para nuestra aplicación. No es buena idea que las aplicaciones que tenemos se ejecuten con el usuario ubuntu. Ya que este usuario puede adquirir privilegios de administrador. Los ciber delincuetes estan ahi asechando. Lo ideal es hacer un usuario por aplicacion. Aqui por acortar tiempo usaremos un solo usuario.

Vamos a crear un usuario, luego vamos a bloquear la cuenta del usuario. Osea que desde fuera nadie pueda autenticarse. Luego nos vamos a convertir en ese usuario. Luego ya podemos empezar el despliegue de la aplicación.

## Creación del usuario:

```bash
sudo adduser apps
```

Nos pedira meter el password y la confirmacion, resto de valores darle enter y listo.

## Bloquear cuenta del usuario:

```bash
sudo passwd -l apps
```

Comprobamos con otra terminal que no se puede acceder al usuario. Vemos que nos dice permiso denegado.

```bash
ssh apps@3.85.27.213
```

## Convertirnos en usuario de la aplicacion:

```bash
sudo -u apps -i
```

-i es de interactive, nos arranca una nueva consola con el usuario. Vemos como la consola ahora se pone con el usuario apps.

![](../doc/img/w28.png)

Ahora para volver a ubuntu bastaria con hacer un logout:

```bash
logout
```

## Instalación de nvm en el servidor para poder usar node:

Aqui podemos ir al repo para la instalacion, a ver al tratarse de nvm nos vamos a fiar pero tener cuidado con isntalar cosas asi en el servidor.

https://github.com/nvm-sh/nvm

Lo haremos con el usuario apps asi nvm solo estara para el usuario apps:

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
```

Debemos reiniciar la terminal:

```bash
logout
```

Verificamos que tenemos instalado nvm, recordar en el usuario apps:

```bash
nvm -v
```

Vamos a instalar la version de node para la version que vamos a desplegar. Para eso tenemos que saber que version usa nuestra aplicación. La aplicación es esta:

https://github.com/igorantun/node-chat

La clonamos en la carpeta del modulo. Se llama node-chat. Lo hemos clonado a nuestro grthub y lo hemos clonado de ahi siguiendo las instrucciones del profe.

Lo clonamos en nuestro servidor. OJO lo he tenido que clonar usando HTTP. El otro me daba fallo. Este chat funciona con node 16 y solo con esa vesion asi que vamos a instalar esa version.

```bash
nvm install 16
```

Comprobamos que se ha instalado bien:

```bash
node --version
```

## Instalamos dependencias del proyecto de chat:

Nos metemos dentro del proyecto y hacemos:

```bash
npm i
```

Luego viendo la documentacion nos dice que hacer arrancar la aplicacion debemos usar:

```bash
npm start
```

📌 Esto en realidad es el primer paso, comprobar que podemos arrancar la app con el script.

Vemos como arranca el chat en el puerto 3000. Vamos al navegador y con la ip:puerto podemos ver el chat:

http://3.85.27.213:3000/

Esta seria la forma de conectarnos pero vemos que no va. Esto es porque el puerto no esta abierto, nos pasa con que puerto 80 debemos abrirlo.

Recordar vamos al panel de AWS, en donde esta la instancia corriendo, vamos a la pestaña de seguridad, grupos de seguridad, editar reglas de entrada, agregar una regla. Procedemos a agregar la regla para el puerto 3000, igual que la del puerto 80. Tenemos que seleccionar la opcion TCP personalizado. Guardamos la regla.

![](../doc/img/w30.png)

OJO: Recordar tener arrancada la aplicacion en el servidor. Asi evitamos darnos golpes que no carga la app.

Al refrescar la pagina vemos como carga nuestra aplicacion de chat. La probamos y funciona. Ya tenemos el primer paso ahora vamos a ir colocando intermediarios entre la aplicacion y yo. Lo anterior es el primer paso.

Ahora nuestra aplicacion funciona porque la tenemos arrancada nosotros con la consola en ssh, pero si cerramos la consola la app muere. Deja de funcionar. Para esto vamos a usar un gestor de procesos llamado PM2.

## 📌🎯🎯 ** PM2: Instalacion: Segundo paso. OJO ver clase 4-1-desplegar-app-node-mongodb.md🎯🎯🎯 ** Ver reinstalacion de PM2 en la ultima version de node

PM2 estara vigilando la aplicación si se cae este la reiniciara.

Vamos a la web de PM2 para ver su documentación:

https://pm2.io/

https://pm2.keymetrics.io/docs/usage/pm2-doc-single-page/

Vamos a instalarlo: Lo hacemos de manera global en el usuario apps:

```bash
npm install pm2@latest -g
```

```bash
pm2 --version
```

Con solo instalarlo vemos que PM2 ya se ha instalado y esta corriendo como un servicio, lo comprobamos:

```bash
ps aux | grep pm2
```

Aqui vemos que esta arrancado:

```bash
apps@ip-172-31-26-131:~$ ps aux | grep pm2
apps        1209  0.1  4.7 628376 46228 ?        Ssl  17:22   0:00 PM2 v5.3.1: God Daemon (/home/apps/.pm2)
apps        1229  0.0  0.2   7008  2304 pts/2    S+   17:24   0:00 grep --color=auto pm2
```

Ahora le debemos decir a PM2 que vigile nuestra app de nodechat. Le debemos decir el archivo javascript que queremos que arranque. Para ver cual es el punto de arranque de la aplicacion lo vemos en el package.json, como vemos es app.js:

```json
  "scripts": {
    "start": "node app.js"
  },
```

Entonces ejecutamos por consola, ojo dentro de la aplicacion:

```bash
pm2 start app.js
```

![](../doc/img/w31.png)

Con esto en teoria la app ya esta funcionando. Vamos a comprobar que esta todo corriendo bien:

```bash
ps aux | grep chat
```

```bash
pm2 list
```

Para probar si se reinicia sola matamos el proceso, cuando hacemos ps aux vemos el proceso:

```bash
kill 1242
```

```bash
ps aux | grep chat
```

Vemos que sigue arrancada al refrescar en la web y el proceso es otro. PM2 esta funcionando bien. Cuando hacemos un pm2 list vemos que el logo de reinicio las veces que se ha reiniado la app.

Tenemos que hacer otra cosa mas una prueba mas, la anterior era reiniando la app ahora vamos ha reniciar el servidor a ver si PM2 levanta como servicio y levanta la app. Lo hacemos en el usuario ubuntu, el administrador.

```bash
sudo reboot
```

Nos tira fuera esperamos un poco a que reinicie. Con esto no va a funcionar. PM2 no arranca solo, antes al instalarlo se arrancao porque lo instalamos, normalmente una app de este tipo si somos nosotros lo que que lo arrancamos generalmente no arrancan solo.

```bash
ps aux | grep pm2
```

Vemos que no hay ningu proceso de PM2 en marcha. Para esto debemos ejecutar este comando de PM2 dentro del usuario apps:

OJO con este comando vemos donde esta instalada una app:

```bash
which pm2
```

```bash
pm2 startup
```

Cuando hacemos esto vemos que nos dice por comando que debemos ejecutar este script si queremos que PM2 se ejecute cuando se reinicie el servidor. Esto se instala como servicio del sistema.

```bash
sudo env PATH=$PATH:/home/apps/.nvm/versions/node/v16.20.2/bin /home/apps/.nvm/versions/node/v16.20.2/lib/node_modules/pm2/bin/pm2 startup systemd -u apps --hp /home/apps
```

Como vemos el comando que nos da va con sudo y con el usuario apps no puedo ejecutar comandos con sudo por lo tanto este comando lo debo ejecutar con el usuario ubuntu que si puede ejecutar comando con sudo.

Nos pasamos al usuario ubuntu y lo ejecutamos. Todo correcto. ahora si reiniciamos el servidor este deberia ejecutarse al arrancar el servidor. Probamos el chat y funciona, pm2 se ha arrancado.

Asi paramos/arancamos la aplicacion usando PM2:

```bash
pm2 stop app
```

```bash
pm2 start app
```

## 📌 Ejecutar en puerto 80: Tercer paso:

Queremos que nuestro chat se ejecute en el puerto 80 y no en el 3000. El puerto 80 es el puerto por defecto asi ejecutamos la ip sin tener que poder el puerto.

NOTA: Solo los usuarios root o administradores pueden ejecutar cosas en puerto por debajo del 1024, osea nuestro usuario apps no podria ejecutar la app por medio de ese puerto.

Nosotros en el puerto 80 ya tenemos corriendo a NGNIX. Usaremos a NGINX para poder ejecutar nuestra app que corre con el usuario apps que no es root. Aprenderemos a usar a NGINX como proxy inverso.

## NGINX como proxy inverso:

Ser un proxy inverso es que NGINX esta escuchando en el puerto 80 y cuando le llega una peticion a una url le redirije a ese servidio, bien sea el chat bien sea un blog, etc. El esta en el medio.

Vamos a modificar que nos muestre por ip, quitaremos el estatico de bootstraps. Lo quitamos del acceso directo que tenemos en enabled.

```bash
sudo rm /etc/nginx/sites-enabled/default
```

Tenemos que ir a la configuracion de NGINX que tenemos en /etc/nginx/sites-available tenemos que configurar nuestro archivo para el nuevo sitio:

```bash
cd /etc/nginx/sites-available/
```

```bash
sudo nano chat
```

NOTA: Cuando hacemos peticiones por IP ponemos default:

```bash
server {
        listen 80 default;

        location / {
                proxy_pass http://127.0.0.1:3000;
                proxy_redirect off;
        }
}
```

Vamos ahora a crear el acceso directo: Eso lo tenemos que hacer en /etc/nginx/sites-enabled/

NOTA: Da igual donde estemos que lo crea:

```bash
sudo ln -s /etc/nginx/sites-available/chat /etc/nginx/sites-enabled/chat-link
```

```bash
sudo nginx -t
```

```bash
sudo systemctl reload nginx
```

Recargamos la pagina y ahi tenemos al chat funcionando con ip. Pero no va bien porque para que funciones tiene que ir la ip:3000. mmmm solo queremos que lo haga con la ip. Tenemos un problema de websocket con nginx. Para solucionarlo debemos agregar unas lineas a nuestro archivo de configuracion de nginx para el chat.

```bash
server {
        listen 80 default;
        location / {
                proxy_pass http://127.0.0.1:3000;
                proxy_redirect off;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "Upgrade";
                proxy_set_header Host $host;
        }
}
```

```bash
sudo nginx -t
```

```bash
sudo systemctl reload nginx
```

Ahora si tenemos funcionando nuestra app con la ip.

OJO vamos a cerrar el puerto 3000 que est abierto en AWS!!!!

Ahora tenemos 2 intermediarios entre el chat y el cliente, PM2 que se encarga de mantener viva la aplicacion y NGNIX como proxy inverso.
