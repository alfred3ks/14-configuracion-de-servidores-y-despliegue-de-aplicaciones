# Temas de los permisos:

Los permisos de linux los podemos representar en:

usuario: rwx
grupo: rwx
otros: rwx

Numero en binarios:

0 -> 000
1 -> 001
2 -> 010
3 -> 011
4 -> 100
5 -> 101
6 -> 110
7 -> 111

Si ejecutamos un chmod 600:

usuario -> 6
grupo -> 0
otros -> 0

El 6 en binario es 110
El 0 en binario es 000

Entonces nos quedaria asi, es como una multiplicacion y nos quedaris asi:

usuario: rwx x 110 = rw-
grupo: rwx x 000 = ---
otros: rwx x 000 = ---

Entonces los permisos quedaria asi:

usuario: rw-
grupo: ---
otros: ---

Hemos dado permisos al usuario de escritura y lectura pero el resto no al grupo ni a otros.

Veamos otro ejemplo:
chmod 755:

7:111
5:101
5:101

usuario: rwx x 111 = rwx
grupo: rwx x 101 = r-x
otros: rwx x 101 = r-x

usuario: rwx
grupo: r-x
otros: r-x

Creo que me parece mas facil decir como por ejemplo chmod u+rwx g+rx o+rx y seria lo mismo. Es mas facil que la representacion binaria.
