# Desplegar PM2 y supervisor en la app de node con mongodb:

Despliegue de la aplicacion de parse.

Tenemos hasta este momento la aplicacion de parse en el usuario apps, para desplegarla sin PM2 y supervisor lo hremos asi:

## Creamos las variables de entorno:

Necesitamos el usuario de la BD de mongo y su contraseña, recordar que esta variable de entorno se genera solo a nivel de usuario cuando salgo del usuario de borra:

```bash
export DATABASE_URI=mongodb://<user>:<password>@127.0.0.1:27017/<nombre-bd>
```

Procedemos a arrancar la aplicacion usando el script de arranque:

```bash
npm start
```

Vemos como la app de parcel arranca sin problemas, recordar esta app esta desplegada en el usuario app, para entrar en el usaremos la instruccion desde ubuntu:

```bash
sudo -u apps -i
```

Cuando despleguemos nuestra aplicacion usando el script debemos probar bien antes de pasar a PM2 o supervisor que la aplicacion arranca bien usando el npm star que vemos que arranca bien y en el packaje.json vemos que tenemos que el comando npm start hace llamado a node index.js si hacemos el arranque asi la app peta. Por eso cuando le pasamos la configuracion a PM2 o supervidor estas petan tambien. Moraleja ojo de probar los script de arranque. Asi evitamos estar dando palos de ciegos.

La estrategia pasa en desplegar la app usando npm start.

Vamos a desinstalar supervisor y usaremos es PM2. Vamos al usuario ubuntu:

```bash
sudo apt purge supervisor
```

Comprobamos que esta KO:

```bash
ps aux | grep supervisor
```

Vamos al usuario apps. Vamos a configurar el archivo de configuracion de PM2 para el chat y parse usando PM2. El archivo ecosystem.config.js:

Vamos a hacerlo con el chat primero a ver si nos funciona bien. Si nos metemos dentro del la aplicacion y arrancamos la app del chat con el script vemos que arranca: OJO el chat va con la version 16 de node.

```bash
npm start
```

NOTA: Si queremos parar y matar el proceso de PM2 usamos:

```bash
pm2 stop all
```

```bash
pm2 delete all
```

Lo que haremos usando el comando npm start dentro del fichero de configuracion para usar PM2: Tenemos que cambiar a la version 21 de node que es dond esta isntalado PM2.

Creamos el esqueleto del archivo ecosystem.config.js:

```bash
pm2 init simple
```

Lo editamos:

```bash
nano ecosystem.config.js
```

Para saber el path de un ejecutable de una version de node:

```bash
nvm which 16
```

```bash
module.exports = {
  apps : [
    {
      name   : "chat",
      cwd    : "/home/apps/node-chat",
      script : "npm",
      args   : "start",
      env    : { PATH: "/home/apps/.nvm/versions/node/v16.20.2/bin:/usr/bin" }
   }
  ]
}
```

env : { PATH: "/home/apps/.nvm/versions/node/v16.20.2/bin:/usr/bin" }

Ejecuta la version de node necesario y ademas comandos de sh,

Para arrancar PM2:

```bash
pm2 start ecosystem.config.js
```

Vemos como funciona ya nuestra app usando PM2 y el archivo de configuracion.

Ahora vamos a agregar la configuracion para la app de parce.

```bash
module.exports = {
  apps : [
    {
      name   : "chat",
      cwd    : "/home/apps/node-chat",
      script : "npm",
      args   : "start",
      env    : { PATH: "/home/apps/.nvm/versions/node/v16.20.2/bin:/usr/bin" }
   },
    {
      name   : "parse",
      cwd    : "/home/apps/parse",
      script : "npm",
      args   : "start",
      env    : {
        PATH: "/home/apps/.nvm/versions/node/v21.6.2/bin:/usr/bin",
        DATABASE_URI: "mongodb://parseusr:1296@22T@127.0.0.1:27017/parsedb"
      }
    }
  ]
}
```

Lo probamos y funciona!!!! a mi me ha funcionado con la version de node 21 y no con la 18 como al profesor.

Lo probamos con el navagador con la siguiente url:

http://3.91.223.233:1337/

Y Yambien usando postman:

http://3.91.223.233:1337/parse/classes/cervezas

Ahora mismos como esta configurado si se llega a reiniciar el servidor pm2 no arrancara las aplicaciones que se hacer cargo nos falta guardar una foto a PM2 para decirle que aplicaciones debe levantar en un reinicio eso lo hacemos con un comando:

```bash
pm2 save
```

OJO cada vez que hacemos un cambio en el archivo de configuracion de PM2 o paramos una aplicacion o un despliegue nuevo tenemos que hacer pm2 save.

Ahora falta hacer el despliegue sin mostrar el puerto solo usando la ip.

## 🎯🎯🎯 Despliegue usando la IP: Tercer paso.

Vamos a usar a NGINX como proxy inverso. Vale en la DNS tenemos la aplicacion de react y el chat en la ip.

Con lo que nos da AWS colgamos una app en el ip o bien en la DNS. Ya esta.
Queremos que al entrar a la IP entremos al chat y a parse.

Para esto vamos a la configuracion de ngnix que tenemos en /etc/nginx/sites-available:

```bash
server {
    listen 80 default;
    location /parse {
      proxy_pass http://127.0.0.1:1337;
      proxy_redirect off;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_set_header Host $host;
    }

    location / {
      proxy_pass http://127.0.0.1:3000;
      proxy_redirect off;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_set_header Host $host;
  }
}
```

Comprobamos la configuracion de Nginx:

```bash
sudo ngnix -t
```

```bash
sudo systemctl reload nginx
```

Ahora hacemos con postman las peticiones asi quitando el puerto:

GET http://3.91.223.233/parse/classes/cervezas

http://3.91.223.233

Vemos como funciona correctamente. Sigue funcionando, y el chat tambien. Ya tenemos el fichero de configuración de nginx actualizado.

Ahora cerramos el puerto en AWS que teniamos abierto para el despliegue de parse.

🎯🎯🎯🎯 Nos queda una cosa vamos con el enunciado de la practica. Esto es el apartado 3 del ejercicio 1 de la practica.

Lo que haremos es que node haga lo que mejor sabe hacer y para servir archivos estaticos lo haremos con nginx. Por ejemplo los archivos css, los js, cuando hacemos una peticion vemos que esos archivos estan servidos por express.

![](../doc/img/w39.png)

Lo que haremos es que todos estos archivos no los sirva node sino nginx. Que es mucho mejor que node para hacer estas cosas. Para esto tenemos que en el archivo de configuracion de nginx.

Las validaciones lo haremos con expresiones regulares. Lo vemos en el pdf de nginx de la clase, al final:

```bash
 # sirviendo archivos estáticos desde nginx
   location ~ ^/(css/|img/|js/|sounds/) {
      root /var/www/app/public; # ruta base al directorio de estáticos
      access_log off; # no dejar log de acceso a estáticos (no aportan info)
      expires max; # máximo de expiración (cache)
   }
```

En nuestra aplicacion de node chat la ruta donde estan los estaticos los tenemos en:

public

Vamos al archivo de configuracion de nginx que los tenemos en /etc/nginx/sites-available/chat. Lo hacemos con el suuario ubuntu ya que es la configuarcion de nginx:

```bash
sudo nano /etc/nginx/sites-available/chat
```

```bash
server {
  listen 80 default;
  location /parse {
          proxy_pass http://127.0.0.1:1337;
          proxy_redirect off;
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "Upgrade";
          proxy_set_header Host $host;
  }

  location ~ ^/(css|img|sounds|fonts|js)/ {
          root /home/apps/node-chat/public;
          access_log off;
          expires max;
          add_header X-Owner alfred3ks;
  }

  location / {
          proxy_pass http://127.0.0.1:3000;
          proxy_redirect off;
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "Upgrade";
          proxy_set_header Host $host;
  }
}
```

Hemos agregado este codigo de configuracion al archivo y debe ir en ese orden y localizacion, ver el apartado add_header es lo que nos pide en la practica.

```bash
location ~ ^/(css|img|sounds|fonts|js)/ {
  root /home/apps/node-chat/public;
  access_log off;
  expires max;
  add_header XOwner alfred3ks;
}
```

```bash
sudo nginx -t
```

```bash
sudo systemctl reload nginx
```

Aqui al refrescar la pagina no nos carga los archivos por problemas de permisos. Vamos a ver los permisos de nginx:

```bash
ps aux | grep nginx
```

```bash
root         432  0.0  0.6  55412  6052 ?        Ss   Feb23   0:00 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
www-data   12823  0.0  0.6  55892  6540 ?        S    17:50   0:00 nginx: worker process
```

El usuario de NGINX es www-data.
Empezamos a revisar los permisos de las carpetas, de nuestro proyecto node-chat

```bash
ubuntu@ip-172-31-26-131:~$ sudo ls -l /home/apps/node-chat/public
```

```bash
drwxrwxr-x 2 apps apps 4096 Feb 21 19:42 css
drwxrwxr-x 2 apps apps 4096 Feb 21 19:42 fonts
drwxrwxr-x 2 apps apps 4096 Feb 21 19:42 img
drwxrwxr-x 2 apps apps 4096 Feb 21 19:42 js
drwxrwxr-x 2 apps apps 4096 Feb 21 19:42 sounds
```

Vemos que para el usuario otros tiene permisos vamos buscando mas abajo:

```bash
ubuntu@ip-172-31-26-131:~$ sudo ls -l /home/apps/node-chat/
```

```bash
ubuntu@ip-172-31-26-131:~$ sudo ls -l /home/apps/
```

```bash
ubuntu@ip-172-31-26-131:~$ sudo ls -l /home/
```

```bash
ubuntu@ip-172-31-26-131:~$ sudo ls -l /home/
total 16
drwxr-x--- 11 apps   apps   4096 Feb 25 17:35 apps
drwxr-xr-x  4 goku   goku   4096 Feb 18 17:03 goku
drwxr-x---  7 ubuntu ubuntu 4096 Feb 24 16:15 ubuntu
drwxr-x---  3 vegeta vegeta 4096 Feb 18 17:04 vegeta
```

Vemos que usuario apps no tiene permisos de lectura ni ejecusion para otros usuarios.

Por eso nginx que su usuario es www-data intenta entrar en los archivos de public no tiene permisos por tanto permisos denegados. Solucion darle permisos a esa carpeta.

A la carpeta apps vamos a dar permisos de ejecucion y lectura a otros:

```bash
sudo chmod o+rx /home/apps
```

```bash
ubuntu@ip-172-31-26-131:~$ sudo chmod o+rx /home/apps
ubuntu@ip-172-31-26-131:~$ sudo ls -l /home/
total 16
drwxr-xr-x 11 apps   apps   4096 Feb 25 17:35 apps
drwxr-xr-x  4 goku   goku   4096 Feb 18 17:03 goku
drwxr-x---  7 ubuntu ubuntu 4096 Feb 24 16:15 ubuntu
drwxr-x---  3 vegeta vegeta 4096 Feb 18 17:04 vegeta
```

Recargamos la web y todo va de perlas. Y vemos en la respuesta que los estaticos vienen con la cabecera que le hemos puesto y los sirve nginx:

![](../doc/img/w40.png)

Hasta aqui ya tenemos todos los conocimientos para hacer toda la practica entera.
